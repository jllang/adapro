#!/bin/bash

mkdir -p ../dist/doc;
dia ControlClassDiagram.dia --export=ControlClassDiagram.eps && \
    dia DataClassDiagram.dia --export=DataClassDiagram.eps && \
    dia StateDiagram.dia --export=StateDiagram.eps && \
    dia SequenceDiagram1.dia --export=SequenceDiagram1.eps && \
    dia SequenceDiagram2.dia --export=SequenceDiagram2.eps && \
    dia SequenceDiagram3.dia --export=SequenceDiagram3.eps && \
#    dia SequenceDiagram4.dia --export=SequenceDiagram4.eps && \
    dia SequenceDiagram5.dia --export=SequenceDiagram5.eps && \
    dia CommunicationDiagram1.dia --export=CommunicationDiagram1.eps && \
    pdflatex -halt-on-error Documentation.tex 2>&1 /dev/null && \
    pdflatex -halt-on-error Documentation.tex 2>&1 /dev/null; # TOC and stuff requires two compilations.

# Now we just move the PDF and remove the useless extra files. (Note that if the
# compilation fails for some reason, it might be worthwhile to run 'pdflatex
# TechnicalDocumentation.tex' instead of this script.)

mv -f ./Documentation.pdf ../dist/doc;
#rm -f ./*.eps;
rm -f ./*.pdf;
rm -f ./*.aux;
rm -f ./*.toc;
rm -f ./*.lof;
rm -f ./*.lot;
rm -f ./*.log;
rm -f ./*.out;

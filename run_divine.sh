#!/bin/bash

# This script is based on the original idea of Burno Girin, with modifications:
# https://brunogirin.blogspot.com/2010/09/memory-usage-graphs-with-ps-and-gnuplot.html

# Arguments: 
# 1. check / verify for selecting between divine check and divine verify;
# 2. Name of the model: worker / supervisor / session; and
# 3. Polling interval in seconds for the memory/CPU graph in seconds.

usage="Usage: run_divine.sh {check | verify} {worker | supervisor | session}   \
        {<thread count>} {<memory limit, GiB>} {<sampling time>}"
if [[ ${1,,} =~ ^(check|verify)$ && \
    ${2,,} =~ ^(worker|supervisor|session)$ && \
    $3 =~ ^[0-9]+$ && $3 > 0 && \
    $4 =~ ^[0-9]+$ && $4 > 0 && \
    $5 =~ ^[0-9]+$ && $5 > 0 ]]
then    
    adapro_files="src/control/Metronome.cpp                                    \
        src/control/Thread.cpp                                                 \
        src/control/Supervisor.cpp                                             \
        src/control/Session.cpp                                                \
        src/library/ConfigurationUtils.cpp                                     \
        src/library/MetronomeFactory.cpp                                       \
        src/library/StringUtils.cpp"
    name="${2^}Model"
    tmp_dir="/tmp/divine"
    dist_dir="dist/reports/divine/$name/${1,,}"
    extra_options=""
    [[ ${1,,} == "verify" ]] && extra_options="-o nofail:malloc"               \
                             || extra_options="--leakcheck state,exit"
    mkdir -p $tmp_dir
    mkdir -p $dist_dir
    divine cc -std=c++14 -DEXCLUDE_DIM -DVERIFY                                \
        -o "$tmp_dir/$name.bc" $adapro_files "models/divine/$name.cpp" -lm
    /usr/bin/time -v -o "$dist_dir/$name.log"                                  \
        divine ${1,,} --report-filename "$dist_dir/$name.report"               \
        --threads "$3" --max-memory "$4G" $extra_options "$tmp_dir/$name.bc" &
    while [[ `pgrep -cx divine` == 0 ]]; do
        sleep 0.01
    done
    while [[ `pgrep -cx divine` == 1 ]]; do
        sample=`ps -C divine -o %cpu=,rss=`
        parts=($sample)
        echo ${parts[0]} `bc <<< "scale=3;${parts[1]}/1048576"` >> "$tmp_dir/divine.log"
        gnuplot models/divine/plot.plt
        sleep $5
    done
    mv "$tmp_dir/graph.png" "$dist_dir/$name.png"
    rm -rf "$tmp_dir"
else 
    echo $usage;
    exit 1
fi
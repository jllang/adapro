set term png small size 800,600
set output "/tmp/divine/graph.png"

set ylabel "% CPU Time"
set y2label "Resident Memory Size, GiB"
set xlabel "Time (Sample Count)"

set ytics nomirror
set y2tics nomirror in

set yrange [0:400]
set y2range [0:*]

plot "/tmp/divine/divine.log" using 1 with lines axes x1y1 title "% CPU Time", \
     "/tmp/divine/divine.log" using 2 with lines axes x1y2 title "Resident Memory Size, GiB"

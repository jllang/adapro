
/*
 * File:   ReportTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:26:47
 */

#ifndef ADAPRO_REPORT_TEST_HPP
#define ADAPRO_REPORT_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ReportTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ReportTest);

    CPPUNIT_TEST(test_show);

    CPPUNIT_TEST_SUITE_END();

public:
    ReportTest();
    virtual ~ReportTest();
    void setUp();
    void tearDown();

private:
    void test_show();
};

#endif /* ADAPRO_REPORT_TEST_HPP */


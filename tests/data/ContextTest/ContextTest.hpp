/*
 * File:   ContextTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22.5.2017, 12:22:26
 */

#ifndef ADAPRO_CONTEXT_TEST_HPP
#define ADAPRO_CONTEXT_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ContextTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ContextTest);

    CPPUNIT_TEST(test_ctor_dtor);

    CPPUNIT_TEST_SUITE_END();

public:
    ContextTest();
    virtual ~ContextTest();
    void setUp();
    void tearDown();

private:
    void test_ctor_dtor();
};

#endif /* ADAPRO_CONTEXT_TEST_HPP */


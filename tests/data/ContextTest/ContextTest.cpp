/*
 * File:   ContextTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 May 2017, 12:22:26
 */

#include <string>
#include <list>
#include <functional>
#include <memory>
#include "ContextTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Worker.hpp"
#include "../../../headers/control/Session.hpp"
#include "../../../headers/data/Context.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "headers/DIM/Typedefs.hpp"
#ifndef EXCLUDE_DIM
#include "../../../headers/DIM/Wrapper.hpp"
#endif

CPPUNIT_TEST_SUITE_REGISTRATION(ContextTest);

ContextTest::ContextTest() {}

ContextTest::~ContextTest() {}

void ContextTest::setUp() {}

void ContextTest::tearDown() {}

void ContextTest::test_ctor_dtor()
{
    ADAPRO::Data::Context ctx_1
    {
            "ContextTest"
            , "5.0.0"
            , 0
            , nullptr
            , std::list<ADAPRO::Data::worker_factory_t>{}
            , std::list<std::string>{}
            , ADAPRO::Library::make_default_configuration()
            , true
            , true
            , false
            , ADAPRO::Library::default_relation
            , ADAPRO::Library::default_signal_callback
#ifndef EXCLUDE_DIM
            , std::list<ADAPRO::DIM::command_factory_t>{}
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
#endif
    };
    ADAPRO::Data::Context ctx_2{std::move(ctx_1)};
//    ADAPRO::Data::Context ctx_3;
//    ctx_3 = std::move(ctx_2);
    CPPUNIT_ASSERT(ctx_1.application_name == "");
//    CPPUNIT_ASSERT(ctx_2.application_name == "");
    CPPUNIT_ASSERT(ctx_2.application_name == "ContextTest");
//    CPPUNIT_ASSERT(ctx_3.application_name == "ContextTest");
//    CPPUNIT_ASSERT(!ctx_3.signal_callback(5));
}

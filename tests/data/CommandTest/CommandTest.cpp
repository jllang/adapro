
/*
 * File:   CommandTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:16:16
 */

#include <string>
#include <stdexcept>
#include "CommandTest.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/data/Command.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(CommandTest);

using namespace std;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

CommandTest::CommandTest() {}

CommandTest::~CommandTest() {}

void CommandTest::setUp() {}

void CommandTest::tearDown() {}

void CommandTest::test_symbol()
{
    CPPUNIT_ASSERT("Stt"    == symbol<Command>(START));
    CPPUNIT_ASSERT("Stp"    == symbol<Command>(STOP));
    CPPUNIT_ASSERT("Pau"    == symbol<Command>(PAUSE));
    CPPUNIT_ASSERT("Cnt"    == symbol<Command>(CONTINUE));
    CPPUNIT_ASSERT("Abr"    == symbol<Command>(ABORT));
    CPPUNIT_ASSERT("Stt"    == symbol<Command>((Command) 0x01));
    CPPUNIT_ASSERT("Stp"    == symbol<Command>((Command) 0x02));
    CPPUNIT_ASSERT("Pau"    == symbol<Command>((Command) 0x04));
    CPPUNIT_ASSERT("Cnt"    == symbol<Command>((Command) 0x08));
    CPPUNIT_ASSERT("Abr"    == symbol<Command>((Command) 0x10));
    bool exception(false);
    try
    {
        symbol<Command>((Command) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void CommandTest::test_show()
{
    CPPUNIT_ASSERT("START"      == show<Command>(START));
    CPPUNIT_ASSERT("STOP"       == show<Command>(STOP));
    CPPUNIT_ASSERT("PAUSE"      == show<Command>(PAUSE));
    CPPUNIT_ASSERT("CONTINUE"   == show<Command>(CONTINUE));
    CPPUNIT_ASSERT("ABORT"      == show<Command>(ABORT));
    CPPUNIT_ASSERT("START"      == show<Command>((Command) 0x01));
    CPPUNIT_ASSERT("STOP"       == show<Command>((Command) 0x02));
    CPPUNIT_ASSERT("PAUSE"      == show<Command>((Command) 0x04));
    CPPUNIT_ASSERT("CONTINUE"   == show<Command>((Command) 0x08));
    CPPUNIT_ASSERT("ABORT"      == show<Command>((Command) 0x10));
    bool exception(false);
    try
    {
        show<Command>((Command) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

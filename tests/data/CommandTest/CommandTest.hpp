
/*
 * File:   CommandTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:16:16
 */

#ifndef ADAPRO_COMMAND_TEST_HPP
#define ADAPRO_COMMAND_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class CommandTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(CommandTest);

    CPPUNIT_TEST(test_symbol);
    CPPUNIT_TEST(test_show);

    CPPUNIT_TEST_SUITE_END();

public:
    CommandTest();
    virtual ~CommandTest();
    void setUp();
    void tearDown();

private:
    void test_symbol();
    void test_show();
};

#endif /* ADAPRO_COMMAND_TEST_HPP */


/*
 * File:   MetronomeFactoryTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 May 2017, 14:01:18
 */

#include <cstdint>
#include <chrono>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Metronome.hpp"
#include "../../../headers/library/MetronomeFactory.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "MetronomeFactoryTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(MetronomeFactoryTest);

using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

Logger MetronomeFactoryTest::LOGGER(FATAL);

MetronomeFactoryTest::MetronomeFactoryTest() {}

MetronomeFactoryTest::~MetronomeFactoryTest() {}

void MetronomeFactoryTest::setUp() {}

void MetronomeFactoryTest::tearDown() {}

void MetronomeFactoryTest::test_get_metronome()
{
    Metronome& m1(get_metronome(LOGGER));
    Metronome& m2(get_metronome(LOGGER));
    Metronome& m3(get_metronome(LOGGER, 500));
    Metronome& m4(get_metronome(LOGGER, 500));
    Metronome& m5(get_metronome(LOGGER, 500, 1));
    Metronome& m6(get_metronome(LOGGER, 500, 1));
    Metronome& m7(get_metronome(LOGGER, 250, 1));
    Metronome& m8(get_metronome(LOGGER, 250, 1));
    CPPUNIT_ASSERT(m1.name == "Metronome<1000>-0");
    CPPUNIT_ASSERT(m2.name == "Metronome<1000>-0");
    CPPUNIT_ASSERT(m3.name == "Metronome<500>-1");
    CPPUNIT_ASSERT(m4.name == "Metronome<500>-1");
    CPPUNIT_ASSERT(m5.name == "Metronome<500>-1");
    CPPUNIT_ASSERT(m6.name == "Metronome<500>-1");
    CPPUNIT_ASSERT(m7.name == "Metronome<250>-2");
    CPPUNIT_ASSERT(m8.name == "Metronome<250>-2");
    CPPUNIT_ASSERT(m1.tick_duration == chrono::milliseconds(1000));
    CPPUNIT_ASSERT(m2.tick_duration == chrono::milliseconds(1000));
    CPPUNIT_ASSERT(m3.tick_duration == chrono::milliseconds(500));
    CPPUNIT_ASSERT(m4.tick_duration == chrono::milliseconds(500));
    CPPUNIT_ASSERT(m5.tick_duration == chrono::milliseconds(500));
    CPPUNIT_ASSERT(m6.tick_duration == chrono::milliseconds(500));
    CPPUNIT_ASSERT(m7.tick_duration == chrono::milliseconds(250));
    CPPUNIT_ASSERT(m8.tick_duration == chrono::milliseconds(250));
    CPPUNIT_ASSERT(m1.preferred_core == -1);
    CPPUNIT_ASSERT(m2.preferred_core == -1);
    CPPUNIT_ASSERT(m3.preferred_core == -1);
    CPPUNIT_ASSERT(m4.preferred_core == -1);
    CPPUNIT_ASSERT(m5.preferred_core == -1);
    CPPUNIT_ASSERT(m6.preferred_core == -1);
    CPPUNIT_ASSERT(m7.preferred_core == 1);
    CPPUNIT_ASSERT(m8.preferred_core == 1);
}

/*
 * File:   ConfigurationTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 17 January 2017, 15:38:52
 */

#ifndef ADAPRO_CONFIGURATION_UTILS_TEST_HPP
#define ADAPRO_CONFIGURATION_UTILS_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ConfigurationUtilsTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(ConfigurationUtilsTest);

    CPPUNIT_TEST(test_query);
    CPPUNIT_TEST(test_import_configuration_with_nonexisting_file);
    CPPUNIT_TEST(test_import_configuration_with_multiple_files);
    CPPUNIT_TEST(test_import_configuration_with_too_few_tokens);
    CPPUNIT_TEST(test_import_configuration_with_too_many_tokens);
    CPPUNIT_TEST(test_import_configuration_with_key_redefinition);
    CPPUNIT_TEST(test_import_configuration_with_malformed_key);
    CPPUNIT_TEST(test_import_configuration_with_unknown_key);
    CPPUNIT_TEST(test_import_configuration_with_relation_failure);
    CPPUNIT_TEST(test_import_configuration_with_default_relation);
    CPPUNIT_TEST(test_import_configuration_with_custom_relation);
    CPPUNIT_TEST(test_import_configuration_with_different_whitespace);
    CPPUNIT_TEST(test_export_configuration);
    CPPUNIT_TEST(test_export_configuration_tabulated);
    CPPUNIT_TEST(test_export_import_configuration);
    CPPUNIT_TEST(test_print_configuration);

    CPPUNIT_TEST_SUITE_END();

public:
    ConfigurationUtilsTest();
    virtual ~ConfigurationUtilsTest();
    void setUp();
    void tearDown();

private:
    void test_query();
    void test_import_configuration_with_nonexisting_file();
    void test_import_configuration_with_multiple_files();
    void test_import_configuration_with_too_few_tokens();
    void test_import_configuration_with_too_many_tokens();
    void test_import_configuration_with_key_redefinition();
    void test_import_configuration_with_malformed_key();
    void test_import_configuration_with_unknown_key();
    void test_import_configuration_with_relation_failure();
    void test_import_configuration_with_default_relation();
    void test_import_configuration_with_custom_relation();
    void test_import_configuration_with_different_whitespace();
    void test_export_configuration_with_illegal_keys();
    void test_export_configuration_with_illegal_values();
    void test_export_configuration();
    void test_export_configuration_tabulated();
    void test_export_import_configuration();
    void test_print_configuration();
};

#endif /* ADAPRO_CONFIGURATION_UTILS_TEST_HPP */


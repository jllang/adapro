
/*
 * File:   TaskTypeTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:57:58
 */

#include <string>
#include <stdexcept>
#include "TaskTypeTest.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/DIM/TaskType.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(TaskTypeTest);

using namespace std;
using namespace ADAPRO::DIM;
using namespace ADAPRO::Library;

TaskTypeTest::TaskTypeTest() {}

TaskTypeTest::~TaskTypeTest() {}

void TaskTypeTest::setUp() {}

void TaskTypeTest::tearDown() {}

void TaskTypeTest::test_show()
{
    CPPUNIT_ASSERT("PUBLISH_SERVICE"        == show<TaskType>(PUBLISH_SERVICE));
    CPPUNIT_ASSERT("PUBLISH_COMMAND"        == show<TaskType>(PUBLISH_COMMAND));
    CPPUNIT_ASSERT("UPDATE_SERVICE"         == show<TaskType>(UPDATE_SERVICE));
    CPPUNIT_ASSERT("SUBSCRIBE_TO_SERVICE"   ==
            show<TaskType>(SUBSCRIBE_TO_SERVICE));
    CPPUNIT_ASSERT("CALL_COMMAND"           == show<TaskType>(CALL_COMMAND));
    CPPUNIT_ASSERT("OVERWRITE_TIMESTAMP"    ==
            show<TaskType>(OVERWRITE_TIMESTAMP));
    bool exception(false);
    try
    {
        show<TaskType>((TaskType) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void TaskTypeTest::test_state_mask()
{
    CPPUNIT_ASSERT(0x01 == state_mask(PUBLISH_SERVICE));
    CPPUNIT_ASSERT(0x01 == state_mask(PUBLISH_COMMAND));
    CPPUNIT_ASSERT(0x0C == state_mask(UPDATE_SERVICE));
    CPPUNIT_ASSERT(0x0D == state_mask(SUBSCRIBE_TO_SERVICE));
    CPPUNIT_ASSERT(0x0D == state_mask(CALL_COMMAND));
    CPPUNIT_ASSERT(0x0C == state_mask(OVERWRITE_TIMESTAMP));
    bool exception(false);
    try
    {
        state_mask((TaskType) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}


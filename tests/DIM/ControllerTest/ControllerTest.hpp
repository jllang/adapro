/*
 * File:   ControllerTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 1 June 2017, 9:12:01
 */

#ifndef ADAPRO_DIM_CONTROLLER_TEST_HPP
#define ADAPRO_DIM_CONTROLLER_TEST_HPP

#include <cstdint>
#include <map>
#include <string>
#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/data/Typedefs.hpp"

class ControllerTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ControllerTest);

    CPPUNIT_TEST(test_publish_service);
    CPPUNIT_TEST(test_publish_command);
    CPPUNIT_TEST(test_update_service);
    CPPUNIT_TEST(test_subscribe_to_service);
    CPPUNIT_TEST(test_call_command);
    CPPUNIT_TEST(test_overwrite_timestamp);
    CPPUNIT_TEST(test_illegal_task_states);

    CPPUNIT_TEST_SUITE_END();

public:
    ControllerTest();
    virtual ~ControllerTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    static ADAPRO::Data::config_t CONFIGURATION;
    static uint32_t SUPERVISOR_STATE;
    static ADAPRO::Control::Supervisor SUPERVISOR;

    void test_publish_service();
    void test_publish_command();
    void test_update_service();
    void test_subscribe_to_service();
    void test_call_command();
    void test_overwrite_timestamp();
    void test_illegal_task_states();
};

#endif /* ADAPRO_DIM_CONTROLLER_TEST_HPP */


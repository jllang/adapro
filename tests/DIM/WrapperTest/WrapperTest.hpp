/*
 * File:   WrapperTest.hpp
 * Author: John Lång (john.lang@mykolab.com)
 *
 * Created on 1 September 2020, 12:52
 */

#ifndef WRAPPERTEST_HPP
#define WRAPPERTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class WrapperTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(WrapperTest);

    CPPUNIT_TEST(testPubSub);

    CPPUNIT_TEST_SUITE_END();

public:
    WrapperTest();
    virtual ~WrapperTest();
    void setUp();
    void tearDown();

private:
    void testPubSub();
};

#endif /* WRAPPERTEST_HPP */


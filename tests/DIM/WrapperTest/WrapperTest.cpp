/*
 * File:   WrapperTest.cpp
 * Author: John Lång (john.lang@mykolab.com)
 *
 * Created on 1 September 2020, 12:52
 */

#include <string>
#include "WrapperTest.hpp"
#include "headers/DIM/Typedefs.hpp"
#include "headers/DIM/SubscriptionType.hpp"
#include "headers/DIM/Wrapper.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(WrapperTest);

using namespace std;
using namespace ADAPRO::DIM;

WrapperTest::WrapperTest() {}

WrapperTest::~WrapperTest() {}

void WrapperTest::setUp() {}

void WrapperTest::tearDown() {}

void WrapperTest::testPubSub()
{
    string dim_dns_node{"localhost"};
    string name{"test_service"};
    string description{"I:1"};
    int server_value{23};
    int client_value{0};
    Wrapper::common_set_dim_dns_node(dim_dns_node);
    Wrapper::server_add_service
    (
            name,
            description,
            (void*) &server_value,
            sizeof(int)
    );
    Wrapper::client_subscribe
    (
            name,
            SubscriptionType::ONCE_ONLY,
            0,
            (void*) &client_value,
            sizeof(int)
    );
//    CPPUNIT_ASSERT(server_value == client_value);
    // Since we don't have a running DIM DNS node, registering and subscribing
    // to the service fails:
    CPPUNIT_ASSERT(server_value != client_value);
}

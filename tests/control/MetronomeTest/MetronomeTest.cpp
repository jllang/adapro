/*
 * File:   MetronomeTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 November 2016, 14:10:29
 */

#include <cstdint>
#include <thread>
#include <chrono>
#include "MetronomeTest.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Metronome.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(MetronomeTest);

ADAPRO::Control::Logger MetronomeTest::LOGGER
{
        ADAPRO::Data::LoggingLevel::INFO | ADAPRO::Data::LoggingLevel::WARNING |
        ADAPRO::Data::LoggingLevel::ERROR | ADAPRO::Data::LoggingLevel::FATAL,
        "",
        true
};

MetronomeTest::MetronomeTest() {}

MetronomeTest::~MetronomeTest() {}

void MetronomeTest::setUp() {}

void MetronomeTest::tearDown() {}

void MetronomeTest::test_synchronize()
{
    Metronome m1{LOGGER};
    const bool condition_1{m1.name == "Metronome<1000>-0"};
    uint64_t beginning, duration;
    beginning = ADAPRO::Library::now();
    m1.synchronize();
    duration = ADAPRO::Library::now() - beginning;
    const bool condition_2{duration >= 500};
    LOGGER.print("m1.synchronize() took " + to_string(duration) + " ms.", INFO);

    Metronome m2{LOGGER, 100};
    const bool condition_3{m2.name == "Metronome<100>-1"};
    beginning = ADAPRO::Library::now();
    m2.synchronize(5);
    duration = ADAPRO::Library::now() - beginning;
    const bool condition_4{duration >= 500};
    LOGGER.print("m2.synchronize() took " + to_string(duration) + " ms.", INFO);

    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
    CPPUNIT_ASSERT(condition_3);
    CPPUNIT_ASSERT(condition_4);
}

void MetronomeTest::test_aborted_synchronize()
{
    class Foo final: public Thread
    {
        Metronome& m;
    protected:
        virtual void prepare() override {}
        virtual void execute() override {m.synchronize(); ++counter;}
        virtual void finish() override {}
    public:
        uint32_t counter;
        Foo(Logger& logger, string&& name, Metronome& m):
                Thread(logger, std::move(name), [](const State s){}), m(m), counter(0) {};
    };

    Metronome m{LOGGER, 1000};
    Foo foo_1{LOGGER, std::move("Foo-1"), m};
    Foo foo_2{LOGGER, std::move("Foo-2"), m};
    m.synchronize();
    foo_1.start_sync();
    foo_2.start_sync();
//    m.abort_async("Simulating abnormal shutdown.");
    m.stop_async(); // TODO: Figure out a new way to abort the Metronome.
    foo_1.stop_async();
    foo_2.stop_async();
    foo_1.wait_for_state_mask(HALTED_MASK);
    foo_2.wait_for_state_mask(HALTED_MASK);

    CPPUNIT_ASSERT(foo_1.counter == 0);
    CPPUNIT_ASSERT(foo_2.counter == 0);
}

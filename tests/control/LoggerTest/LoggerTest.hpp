
/*
 * File:   LoggerTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 January 2017, 14:05:43
 */

#ifndef ADAPRO_LOGGER_TEST_HPP
#define ADAPRO_LOGGER_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class LoggerTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(LoggerTest);

    CPPUNIT_TEST(test_make_logger);
    CPPUNIT_TEST(test_print);

    CPPUNIT_TEST_SUITE_END();

public:
    LoggerTest();
    virtual ~LoggerTest();
    void setUp();
    void tearDown();

private:
    void test_make_logger();
    void test_print();
};

#endif /* ADAPRO_LOGGER_TEST_HPP */


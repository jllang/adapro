/*
 * File:   ThreadGuardTest.hpp
 * Author: John Lång
 *
 * Created on 26 January 2019, 18:23:29
 */

#ifndef ADAPRO_THREAD_GUARD_TEST_HPP
#define ADAPRO_THREAD_GUARD_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ThreadGuardTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(ThreadGuardTest);

    CPPUNIT_TEST(test);

    CPPUNIT_TEST_SUITE_END();

public:
    ThreadGuardTest();
    virtual ~ThreadGuardTest();
    void setUp();
    void tearDown();

private:
    void test();
};

#endif /* ADAPRO_THREAD_GUARD_TEST_HPP */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ThreadGuardTest.cpp
 * Author: johnny
 *
 * Created on 26.1.2019, 18:23:29
 */

#include <string>
#include <map>
#include <functional>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/ThreadGuard.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "ThreadGuardTest.hpp"

using namespace std;
using namespace ADAPRO::Data;
using namespace ADAPRO::Control;

CPPUNIT_TEST_SUITE_REGISTRATION(ThreadGuardTest);

ThreadGuardTest::ThreadGuardTest() {}

ThreadGuardTest::~ThreadGuardTest() {}

void ThreadGuardTest::setUp() {}

void ThreadGuardTest::tearDown() {}

void ThreadGuardTest::test()
{
    // TODO: Make this work somehow:
//    class TestThread : virtual public ADAPRO::Control::Thread
//    {
//    protected:
//        virtual void prepare() override { print("TestThread::prepare()"); }
//        virtual void execute() override { print("TestThread::execute()"); }
//        virtual void finish() override { print("TestThread::finish()"); }
//    };
//
//    Logger logger;
//    ThreadGuard<TestThread> tg{logger, "TestThread", [](const State s){}, -1};
//    Thread* t_ptr = &tg;
//    t_ptr->start_async();
}

/*
 * File:   AOFStreamTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 5 October 2016, 11:33:07
 */

#ifndef ADAPRO_AOFSTREAM_TEST_HPP
#define ADAPRO_AOFSTREAM_TEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"

class AOFStreamTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(AOFStreamTest);

    CPPUNIT_TEST(test_ctor_and_dtor);
    CPPUNIT_TEST(test_put_to_operator);

    CPPUNIT_TEST_SUITE_END();

public:
    AOFStreamTest();
    virtual ~AOFStreamTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    void test_ctor_and_dtor();
    void test_put_to_operator();
};

#endif /* ADAPRO_AOFSTREAM_TEST_HPP */


/*
 * File:   AOFStreamTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 5 October 2016, 11:33:07
 */
#include <string>
#include <queue>
#include <fstream>
#include <iostream>
#include "AOFStreamTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/AOFStream.hpp"
#include "../../../headers/control/AOFStream.hpp"
#include "../../../headers/library/StringUtils.hpp"
#include "../../../headers/data/Command.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(AOFStreamTest);

ADAPRO::Control::Logger AOFStreamTest::LOGGER{WARNING | ERROR | FATAL, "", true};

AOFStreamTest::AOFStreamTest() {}

AOFStreamTest::~AOFStreamTest() {}

void AOFStreamTest::setUp() {}

void AOFStreamTest::tearDown() {}

void AOFStreamTest::test_ctor_and_dtor()
{
    {
        AOFStream<std::string> output
        {
                LOGGER, std::move("string"),
                std::move("test.txt"),
                -1
        };
        output << "This is just a test string.";
        CPPUNIT_ASSERT(output.filename == "test.txt");
    }
    ifstream input("test.txt");
    CPPUNIT_ASSERT(input.is_open());
    input.close();
    remove("test.txt");
}

void AOFStreamTest::test_put_to_operator()
{
    AOFStream<int> output(LOGGER, std::move("int"),
            std::move("test.txt"));
    output << 1;
    output << 2;
    output << 3;
    output << 4;
    output.close();
    queue<string> expected_lines;
    expected_lines.push("1");
    expected_lines.push("2");
    expected_lines.push("3");
    expected_lines.push("4");
    std::ifstream input("test.txt");
    CPPUNIT_ASSERT(input.is_open());
    string line;
    while (getline(input, line))
    {
        CPPUNIT_ASSERT(expected_lines.front() == line);
        expected_lines.pop();
    }
    CPPUNIT_ASSERT(input.eof());
    CPPUNIT_ASSERT(expected_lines.empty());
    remove("test.txt");
}



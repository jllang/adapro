/*
 * File:   SessionTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 14:25:13
 */

#ifdef __divine__
#include <cassert>
#endif
#include <cstdint>
#include <csignal>
#include <string>
#include <iostream>
#include <list>
#include <map>
#include <utility>
#include <functional>
#include <memory>
#include <thread>
#include <atomic>
#include <mutex>
#include <regex>
#include <iostream>
#include <exception>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/control/Session.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/StatusFlags.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "../../../headers/data/Context.hpp"
#ifndef EXCLUDE_DIM
#include "../../../headers/DIM/Wrapper.hpp"
#endif
#include "SessionTest.hpp"

#ifdef __divine__
#define SESSION_TEST_ASSERT(x) assert(x);
#else
#define SESSION_TEST_ASSERT(x) CPPUNIT_ASSERT(x)
CPPUNIT_TEST_SUITE_REGISTRATION(SessionTest);
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

atomic<size_t> SessionTest::SIGSEGV_COUNT{0};
atomic<size_t> SessionTest::SIGABRT_COUNT{0};
atomic<size_t> SessionTest::SIGTERM_COUNT{0};
atomic<size_t> SessionTest::SIGINT_COUNT{0};
atomic<size_t> SessionTest::SIGILL_COUNT{0};
atomic<size_t> SessionTest::SIGFPE_COUNT{0};
atomic<size_t> SessionTest::SIGHUP_COUNT{0};
static mutex MUTEX;
static condition_variable THROW_PERMITTED;

static void run_exception_thrower()
{
    cout << "Exception Thrower started." << '\n';
    unique_lock<mutex> lock(MUTEX);
    THROW_PERMITTED.wait(lock);
    lock.unlock();
    cout << "Exception Thrower is about to throw..." << '\n';
    // This naughty thread calls abort, making it impossible to test
    // the terminate handler properly:
    throw std::runtime_error("Test unhandled exception.");
}

class ExceptionThrower final
{
    thread backend;
public:
    ExceptionThrower() noexcept: backend(run_exception_thrower)
    {
//        backend.detach();
//        cout << backend.joinable() << '\n';
    }
    ~ExceptionThrower()
    {
        cout << "̃~ExceptionThrower()" << '\n';
        if (backend.joinable()) backend.join();
    }
};

SessionTest::SessionTest() {}

SessionTest::~SessionTest() {}

void SessionTest::setUp()
{
    SIGSEGV_COUNT.store(0, memory_order_release);
    SIGTERM_COUNT.store(0, memory_order_release);
    SIGABRT_COUNT.store(0, memory_order_release);
    SIGINT_COUNT.store(0, memory_order_release);
    SIGILL_COUNT.store(0, memory_order_release);
    SIGFPE_COUNT.store(0, memory_order_release);
#ifdef _POSIX_C_SOURCE
    SIGHUP_COUNT.store(0, memory_order_release);
#endif
}

void SessionTest::tearDown() {}

#define BOILERPLATE(n, update_configuration, action)                           \
    uint8_t exit_code{0};                                                      \
    class TestWorker##n final : public Worker                                  \
    {                                                                          \
    protected:                                                                 \
        virtual void prepare() override {}                                     \
        virtual void execute() override                                        \
        {                                                                      \
            action;                                                            \
        }                                                                      \
        virtual void finish() override {}                                      \
    public:                                                                    \
        TestWorker##n(Logger& logger, const config_t& configuration) noexcept: \
                Worker(logger, std::move("TestWorker_"#n), configuration) {}   \
        virtual ~TestWorker##n() noexcept                                      \
        {                                                                      \
            join();                                                            \
        }                                                                      \
    };                                                                         \
    int argc{4};                                                               \
    char* argv[4]                                                              \
    {                                                                          \
            (char*) "<executable name, to be discarded>",                      \
            (char*) "--adapro_logging_mask=WIFE",                              \
            (char*) "tests/resources/nempty.conf",                             \
            (char*) "--ADAPRO_COLOURS_ENABLED=TRUE"                            \
    };                                                                         \
    const list<ADAPRO::Data::worker_factory_t> worker_factories                \
    {                                                                          \
        [](Logger& logger, const config_t& configuration)                      \
        {                                                                      \
            return make_unique<TestWorker##n>(logger, configuration);          \
        }                                                                      \
    };                                                                         \
    const list<string> configuration_paths                                     \
    {                                                                          \
        "tests/resources/unreachable.conf"                                     \
    };                                                                         \
    config_t configuration{make_default_configuration()};                      \
    configuration[show(MAIN_CORE)]          = "-1";                            \
    configuration[show(LOGGING_MASK)]       = "D";                             \
    configuration[show(DIM_SERVER_ENABLED)] = "FALSE";                         \
    configuration[show(COLOURS_ENABLED)]    = "TRUE";                          \
    function<bool(uint8_t)> signal_callback =                                  \
            [](const uint8_t signal)                                           \
            {                                                                  \
                switch (signal)                                                \
                {                                                              \
                    case SIGSEGV: ++SIGSEGV_COUNT; break;                      \
                    case SIGABRT: ++SIGABRT_COUNT; break;                      \
                    case SIGTERM: ++SIGTERM_COUNT; break;                      \
                    case SIGINT: ++SIGINT_COUNT; break;                        \
                    case SIGILL: ++SIGILL_COUNT; break;                        \
                    case SIGFPE: ++SIGFPE_COUNT; break;                        \
                    case SIGHUP: ++SIGHUP_COUNT; break;                        \
                }                                                              \
                return false;                                                  \
            };                                                                 \
    Session::INITIALIZE(std::move(Context{                                     \
            string("SessionTest no. "#n)                                       \
            , string("5.0.0")                                                  \
            , argc                                                             \
            , argv                                                             \
            , worker_factories                                                 \
            , configuration_paths                                              \
            , configuration                                                    \
            , update_configuration                                             \
            , false                                                            \
            , true                                                             \
            , default_relation                                                 \
            , signal_callback                                                  \
    }));                                                                       \
    exit_code = Session::RUN();

#define SIGNAL_TESTCASE(n, signal, flag)                                       \
void SessionTest::test_##signal##_handler()                                    \
{                                                                              \
    BOILERPLATE(n, false, thread([](){raise(signal);}).detach();pause_async(););\
    size_t sum{0};                                                             \
    sum += SIGSEGV_COUNT.load(memory_order_consume);                           \
    sum += SIGABRT_COUNT.load(memory_order_consume);                           \
    sum += SIGTERM_COUNT.load(memory_order_consume);                           \
    sum += SIGINT_COUNT.load(memory_order_consume);                            \
    sum += SIGILL_COUNT.load(memory_order_consume);                            \
    sum += SIGFPE_COUNT.load(memory_order_consume);                            \
    sum += SIGHUP_COUNT.load(memory_order_consume);                            \
    SESSION_TEST_ASSERT(exit_code == flag);                                    \
    SESSION_TEST_ASSERT(SIGSEGV_COUNT.load(memory_order_consume)    >= 0);     \
    SESSION_TEST_ASSERT(SIGSEGV_COUNT.load(memory_order_consume)    <= 1);     \
    SESSION_TEST_ASSERT(SIGABRT_COUNT.load(memory_order_consume)    >= 0);     \
    SESSION_TEST_ASSERT(SIGABRT_COUNT.load(memory_order_consume)    <= 1);     \
    SESSION_TEST_ASSERT(SIGTERM_COUNT.load(memory_order_consume)    >= 0);     \
    SESSION_TEST_ASSERT(SIGTERM_COUNT.load(memory_order_consume)    <= 1);     \
    SESSION_TEST_ASSERT(SIGINT_COUNT.load(memory_order_consume)     >= 0);     \
    SESSION_TEST_ASSERT(SIGINT_COUNT.load(memory_order_consume)     <= 1);     \
    SESSION_TEST_ASSERT(SIGILL_COUNT.load(memory_order_consume)     >= 0);     \
    SESSION_TEST_ASSERT(SIGILL_COUNT.load(memory_order_consume)     <= 1);     \
    SESSION_TEST_ASSERT(SIGFPE_COUNT.load(memory_order_consume)     >= 0);     \
    SESSION_TEST_ASSERT(SIGFPE_COUNT.load(memory_order_consume)     <= 1);     \
    SESSION_TEST_ASSERT(SIGHUP_COUNT.load(memory_order_consume)     >= 0);     \
    SESSION_TEST_ASSERT(SIGHUP_COUNT.load(memory_order_consume)     <= 1);     \
    SESSION_TEST_ASSERT( signal##_COUNT.load(memory_order_consume)  == 1);     \
    SESSION_TEST_ASSERT(sum  == 1);                                            \
}

#define ASSERT_ABSENCE_OF_SIGNALS                                              \
    SESSION_TEST_ASSERT(SIGSEGV_COUNT.load(memory_order_consume)    == 0);     \
    SESSION_TEST_ASSERT(SIGABRT_COUNT.load(memory_order_consume)    == 0);     \
    SESSION_TEST_ASSERT(SIGTERM_COUNT.load(memory_order_consume)    == 0);     \
    SESSION_TEST_ASSERT(SIGINT_COUNT.load(memory_order_consume)     == 0);     \
    SESSION_TEST_ASSERT(SIGILL_COUNT.load(memory_order_consume)     == 0);     \
    SESSION_TEST_ASSERT(SIGFPE_COUNT.load(memory_order_consume)     == 0);     \
    SESSION_TEST_ASSERT(SIGHUP_COUNT.load(memory_order_consume)     == 0);

void SessionTest::test_worker_crash()
{
//    try
//    {
        BOILERPLATE(1, false, throw std::runtime_error{"test"});
        SESSION_TEST_ASSERT(exit_code == FLAG_WORKER_ERROR);
        ASSERT_ABSENCE_OF_SIGNALS
//    }
//    catch (const invalid_argument& e)
//    {
//        cout << e.what() << '\n';
//    }
}

SIGNAL_TESTCASE(2, SIGSEGV, FLAG_SEGV)
SIGNAL_TESTCASE(3, SIGABRT, FLAG_SIGNAL)
SIGNAL_TESTCASE(4, SIGTERM, FLAG_SIGNAL)
SIGNAL_TESTCASE(5, SIGINT,  FLAG_SIGNAL)
SIGNAL_TESTCASE(6, SIGILL,  FLAG_SIGNAL)
SIGNAL_TESTCASE(7, SIGFPE,  FLAG_SIGNAL)
#ifdef _POSIX_C_SOURCE
SIGNAL_TESTCASE(8, SIGHUP,  FLAG_SIGNAL)
#endif

void SessionTest::test_unsupported_signal()
{
//    BOILERPLATE(9, false, thread([](){raise(SIGPIPE);}).detach(); pause_async(););
    BOILERPLATE(9, false, thread([](){raise(SIGQUIT);}).detach(); pause_async(););
    SESSION_TEST_ASSERT(exit_code == FLAG_SIGNAL);
    ASSERT_ABSENCE_OF_SIGNALS
}

void SessionTest::test_unhandled_exception()
{
    ExceptionThrower e;
    BOILERPLATE(10, false, THROW_PERMITTED.notify_all(); pause_async(););
    SESSION_TEST_ASSERT(exit_code == FLAG_UNHANDLED_EXCEPTION);
    ASSERT_ABSENCE_OF_SIGNALS
}

void SessionTest::test_good_session()
{
    static atomic_int I{0};
    BOILERPLATE(11, false, if (++I >= 3) {
        if (this->configuration.find("TICK_COUNT") == this->configuration.end()
                || this->configuration.at("ADAPRO_COLOURS_ENABLED") == "TRUE")
        {
            stop_async();
        }
        else
        {
            throw runtime_error{"Configuration updated without permission!"};
        }
    });
    SESSION_TEST_ASSERT(I == 3);
    SESSION_TEST_ASSERT(exit_code == 0);
    ASSERT_ABSENCE_OF_SIGNALS
}

void SessionTest::test_good_session_with_configuration_file_access()
{
    static atomic_int J{0};
    BOILERPLATE(12, true,
        if (++J >= stoi(this->configuration.at("TICK_COUNT")))
        {
            if (this->configuration.at("ADAPRO_COLOURS_ENABLED") == "FALSE")
            {
                throw runtime_error{"Command-line arguments ignored!"};
            }
            stop_async();
        }
    );
    SESSION_TEST_ASSERT(J == 3);
    SESSION_TEST_ASSERT(exit_code == 0);
    ASSERT_ABSENCE_OF_SIGNALS
}

/*
 * File:   SupervisorTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 9:30
 */

#include <cstdint>
#include <iostream>
#include <string>
#include <memory>
#include <list>
#include <map>
#include <utility>
#include <thread>
#include <chrono>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Worker.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "../../../headers/data/Typedefs.hpp"
#ifndef EXCLUDE_DIM
#include "../../../headers/DIM/Typedefs.hpp"
#include "../../../headers/DIM/Wrapper.hpp"
#endif
#include "SupervisorTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(SupervisorTest);

using namespace std;
using namespace ADAPRO::Control;

Logger SupervisorTest::LOGGER
{
        ADAPRO::Data::LoggingLevel::WARNING |
        ADAPRO::Data::LoggingLevel::ERROR |
        ADAPRO::Data::LoggingLevel::FATAL,
        "",
        true
};
//Logger SupervisorTest::LOGGER{0xFF};

SupervisorTest::SupervisorTest() {}

SupervisorTest::~SupervisorTest() {}

void SupervisorTest::setUp() {}

void SupervisorTest::tearDown() {}

#define MAKE_THREAD_CLASS(type, sleep_duration, starts_paused, freezes)        \
    class type: public Worker                                                  \
    {                                                                          \
    protected:                                                                 \
        virtual void prepare() override                                        \
        {                                                                      \
            if (starts_paused) {pause_async();}                                \
        }                                                                      \
        virtual void execute() override                                        \
        {                                                                      \
            this_thread::sleep_for(chrono::milliseconds(sleep_duration));      \
        }                                                                      \
        virtual void finish() override                                         \
        {                                                                      \
            if (freezes) {this_thread::sleep_for(chrono::seconds(15));}        \
        }                                                                      \
    public:                                                                    \
        type(Logger& logger, const ADAPRO::Data::config_t& configuration) noexcept:\
            Worker(LOGGER, std::move(#type), configuration) {}                 \
        virtual ~type() noexcept {}                                            \
    };                                                                         \
    ADAPRO::Data::worker_factory_t make_##type =                               \
        [](Logger& logger, const ADAPRO::Data::config_t& configuration) {      \
            return unique_ptr<Worker>(new type(logger, configuration));        \
        };

void SupervisorTest::test_serial_lifetime()
{
    MAKE_THREAD_CLASS(A, 10, false, false);
    MAKE_THREAD_CLASS(B, 10, false, false);
    MAKE_THREAD_CLASS(C, 10, true, false);
    ADAPRO::Data::config_t configuration{ADAPRO::Library::make_default_configuration()};
    configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::SERIALIZE_COMMANDS)] = "TRUE";
#ifndef EXCLUDE_DIM
    uint32_t state{0};
#endif
    Supervisor s
    {
            LOGGER
            , configuration
            , std::move(list<ADAPRO::Data::worker_factory_t>{make_A, make_B, make_C})
#ifndef EXCLUDE_DIM
            , std::move(list<ADAPRO::DIM::command_factory_t>{})
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , &state
#endif
    };

    s.start_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.pause_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.resume_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.stop_sync();
    LOGGER.print(s.report());
}

void SupervisorTest::test_parallel_lifetime()
{
    MAKE_THREAD_CLASS(D, 10, false, false);
    MAKE_THREAD_CLASS(E, 10, false, false);
    MAKE_THREAD_CLASS(F, 10, true, false);
    ADAPRO::Data::config_t configuration{ADAPRO::Library::make_default_configuration()};
    configuration[ADAPRO::Library::show(ADAPRO::Data::Parameter::SERIALIZE_COMMANDS)] = "FALSE";
#ifndef EXCLUDE_DIM
    uint32_t state{0};
#endif
    Supervisor s
    {
            LOGGER
            , configuration
            , std::move(list<ADAPRO::Data::worker_factory_t>{make_D, make_E, make_F})
#ifndef EXCLUDE_DIM
            , std::move(list<ADAPRO::DIM::command_factory_t>{})
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , &state
#endif
    };

    s.start_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.pause_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.resume_sync();
    LOGGER.print(s.report());
    this_thread::sleep_for(chrono::milliseconds(100));
    s.stop_sync();
    LOGGER.print(s.report());
}

void SupervisorTest::test_watchdog_mechanism()
{
    MAKE_THREAD_CLASS(G, 3000, false, false);
    ADAPRO::Data::config_t configuration{ADAPRO::Library::make_default_configuration()};
#ifndef EXCLUDE_DIM
    uint32_t state{0};
#endif
    Supervisor s
    {
            LOGGER
            , configuration
            , std::move(list<ADAPRO::Data::worker_factory_t>{make_G})
#ifndef EXCLUDE_DIM
            , std::move(list<ADAPRO::DIM::command_factory_t>{})
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , ADAPRO::DIM::Wrapper::default_dim_error_handler
            , &state
#endif
    };

    s.start_sync();
    this_thread::sleep_for(chrono::seconds(3));
    const uint64_t missed_checkpoints{s.workers.front()->missed_checkpoints};
    s.stop_sync();
    LOGGER.print(s.report());

    cout << "missed checkpoints: " << missed_checkpoints << endl;
    CPPUNIT_ASSERT(missed_checkpoints >= 2 && missed_checkpoints <= 3);
}

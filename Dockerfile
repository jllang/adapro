FROM debian:stretch-slim
RUN echo "deb http://ftp.ch.debian.org/debian/ stretch main contrib non-free\n\
deb-src http://ftp.ch.debian.org/debian/ stretch main contrib\n\
deb http://security.debian.org/debian-security/ stretch/updates non-free contrib main\n\
deb-src http://security.debian.org/debian-security/ stretch/updates non-free contrib main"\
    > /etc/apt/sources.list
RUN apt-get update && apt-get install -y build-essential gcc g++ make git-core\
                    git libcppunit-dev cppcheck doxygen graphviz dia\
                    texlive-base texlive-fonts-recommended texlive-latex-base\
                    texlive-binaries texlive-latex-extra texlive-science\
                    libczmq-dev wget unzip pkg-config lcov libsystemd-dev gdb\
                    valgrind time gnuplot procps bc

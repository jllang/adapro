#include <cstdint>
#include <unordered_map>
#include <utility>
#include <tuple>
#include <algorithm>
#include "../../headers/control/Metronome.hpp"
#include "../../headers/library/MetronomeFactory.hpp"

#ifndef VERIFY
namespace ADAPRO
{
namespace Control
{
    class Logger;
}
}
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;

// Maybe this should be atomic:
static unordered_map<int, Metronome> METRONOME_CACHE;

Metronome& ADAPRO::Library::get_metronome
(
#ifndef VERIFY
        Logger& logger,
#endif
        const uint32_t tick_length,
        const int preferred_core
)
noexcept
{
    auto result{METRONOME_CACHE.find(tick_length)};
    if (result == METRONOME_CACHE.end())
    {
        return (METRONOME_CACHE.emplace
        (
                std::piecewise_construct,
                std::forward_as_tuple(tick_length),
                std::forward_as_tuple
                (
#ifndef VERIFY
                        logger,
#endif
                        tick_length,
                        preferred_core
                )
        ).first)->second;
    }
    else
    {
        return result->second;
    }
}
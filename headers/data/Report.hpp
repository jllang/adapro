/*
 * File:   Report.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 March 2017, 15:39
 */

#ifndef ADAPRO_REPORT_HPP
#define ADAPRO_REPORT_HPP

#include <string>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * This enumeration lists the choices for reports that ADAPRO can print when
     * running in DIM server mode.
     */
    enum Report
    {
        /**
         * Threads report contains a catenation of outputs from <tt>report</tt>
         * methods of the Threads that were registered to the ADAPRO Session.
         *
         * @see ADAPRO::Control::Thread::report
         */
        THREADS,

        /**
         * Configuration report presents the configuration of the Session.
         */
        CONFIGURATION,

        /**
         * Environment report presents some information related to the runtime
         * environment of the ADAPRO application.
         */
        ENVIRONMENT
    };
}

namespace Library
{
    using namespace ADAPRO::Data;

    template<> inline std::string show(const Report report)
    {
        switch (report)
        {
            case THREADS:       return "threads";
            case CONFIGURATION: return "configuration";
            case ENVIRONMENT:   return "environment";
            default:            throw std::domain_error("Invalid Report.");
        }
    }
}
}

#endif /* ADAPRO_REPORT_HPP */


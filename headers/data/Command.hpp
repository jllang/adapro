/*
 * File:   Command.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 July 2015, 17:31
 */

#ifndef ADAPRO_COMMAND_HPP
#define	ADAPRO_COMMAND_HPP

#include <string>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * The State of a Thread can be manipulated by sending it a Command by
     * using the appropriate public methods.
     *
     * @see ADAPRO::Control::Thread
     */
    enum Command
    {
        /**
         * Sending this command causes the Thread to start executing its task.
         *
         * @see ADAPRO::Control::Thread::start
         * @see ADAPRO::Control::Thread::prepare
         * @see ADAPRO::Control::Thread::execute
         */
        START       = 0x01,

        /**
         * Sending this command causes the Thread to stop executing its task.
         *
         * @see ADAPRO::Control::Thread::stop
         * @see ADAPRO::Control::Thread::finish
         */
        STOP        = 0x02,

        /**
         * Sending this command causes the Thread to temporarily stop executing
         * its task and setting its state to <tt>PAUSED</tt>. Setting the
         * command to <tt>CONTINUE</tt> tells the Thread continue its task. It's
         * also possible to move a <tt>PAUSED</tt> Thread into the state
         * <tt>STOPPING</tt> by setting the command to <tt>STOP</tt>.
         *
         * @see ADAPRO::Control::Thread::pause
         */
        PAUSE       = 0x04,

        /**
         * This is the default command and it causes no state transitions.
         *
         * @see ADAPRO::Control::Thread::resume
         * @see ADAPRO::Control::Thread::execute
         */
        CONTINUE    = 0x08,

        /**
         * Used by the thread object itself in case of an exception. This
         * commands instructs the thread to cease execution.
         *
         * @see ADAPRO::Control::Thread::abort
         * @see ADAPRO::Control::Thread::trap
         * @see ADAPRO::Control::Thread::handle_exception
         * @see ADAPRO::Control::Thread::finish
         */
        ABORT       = 0x10
    };
}

namespace Library
{
    using namespace ADAPRO::Data;

    /**
     * Returns a three-letter abbreviated name of the command.
     *
     * @param command The command.
     * @return The abbreviated name of the command.
     * @throws std::domain_error If applied to an invalid value.
     */
    template<> inline std::string symbol(const Command command)
    {
        switch (command)
        {
            case Command::START:     return "Stt";
            case Command::STOP:      return "Stp";
            case Command::CONTINUE:  return "Cnt";
            case Command::PAUSE:     return "Pau";
            case Command::ABORT:     return "Abr";
            default: throw std::domain_error("Invalid State.");
        }
    }


    /**
     * A straightforward conversion from <tt>ADAPRO::Data::Command</tt> to
     * <tt>std::string</tt>. Each Command is mapped to its unqualified name in
     * upper case.
     *
     * @param command A Command.
     * @return      The corresponding string.
     * @throws std::domain_error If applied to an invalid value.
     */
    template<> inline std::string show(const Command command)
    {
        switch (command)
        {
            case Command::START:     return "START";
            case Command::STOP:      return "STOP";
            case Command::CONTINUE:  return "CONTINUE";
            case Command::PAUSE:     return "PAUSE";
            case Command::ABORT:     return "ABORT";
            default:        throw std::domain_error("Invalid Command.");
        }
    }
}

}

#endif	/* ADAPRO_COMMAND_HPP */


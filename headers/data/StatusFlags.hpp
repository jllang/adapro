
/*
 * File:   StatusFlags.hpp
 * Author: john Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 February 2017, 11:36
 */

#ifndef ADAPRO_STATUS_FLAGS_HPP
#define ADAPRO_STATUS_FLAGS_HPP

#include <cstdint>

namespace ADAPRO
{
namespace Data
{
    /**
     * Status flag for unhandled exceptions. Used as the default error status
     * flag.
     */
    constexpr uint8_t FLAG_UNHANDLED_EXCEPTION{0x01};

    /**
     * Status flag for malformed configuration file.
     */
    constexpr uint8_t FLAG_BAD_CONFIG{0x02};

    /**
     * Status flag for segmentation violation.
     */
    constexpr uint8_t FLAG_SEGV{0x04};

    /**
     * Status flag for a signal (other tan SIGSEGV).
     */
    constexpr uint8_t FLAG_SIGNAL{0x08};

    /**
     * Status flag for a DIM error.
     */
    constexpr uint8_t FLAG_DIM_ERROR{0x10};

    /**
     * Status flag for the death of one or more Worker Threads.
     */
    constexpr uint8_t FLAG_WORKER_ERROR{0x20};
}
}

#endif /* ADAPRO_STATUS_FLAGS_HPP */


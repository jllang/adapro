/*
 * File:   LoggingLevel.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 27 April 2017, 16:32
 */

#ifndef ADAPRO_LOGGING_LEVEL_HPP
#define ADAPRO_LOGGING_LEVEL_HPP

#include <cstdint>
#include <string>
#include <utility>
#include <regex>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"
#include "../data/Parameters.hpp"
#include "Parameters.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * Logging level is used for characterising the severity of an event that
     * generates a message using <tt>ADAPRO::Control::Logger</tt>.
     */
    enum LoggingLevel
    {
        /**
         * <tt>ADAPRO_DEBUG</tt> is the lowest logging level. It should be
         * reserved for the use of ADAPRO framework only. Messages of this
         * logging level will be displayed only in the most verbose mode of
         * logging. In compatible shells, if colours are enabled in the Session
         * configuration, the colour of these messages is gray. The symbol of
         * this logging level is <tt>'A'</tt>.
         */
        ADAPRO_DEBUG    = 0x01,

        /**
         * Used by the framework to print information and warning messages about
         * Thread state transitions. In compatible shells, if colours are
         * enabled in the Session configuration, the colour of these messages is
         * blue. The symbol of this logging level is <tt>'M'</tt>.
         */
        FSM             = 0x02,

        /**
         * This is the default logging level. In compatible shells, if colours
         * are enabled in the Session configuration, the colour of these
         * messages is gray. Reserved for user applications.  The symbol of this
         * logging level is <tt>'D'</tt>.
         */
        USER_DEBUG      = 0x04,

        /**
         * Messages with this logging level don't have a special colour code.
         * The symbol of this logging level is <tt>'I'</tt>.
         */
        INFO            = 0x08,

        /**
         * In compatible shells, if colours are enabled in the Session
         * configuration, the colour of these messages is green. The symbol of
         * this logging level is <tt>'S'</tt>.
         */
        SPECIAL         = 0x10,

        /**
         * Used for signalling an unexpected runtime event or exception that is
         * less serious than an event of the level <tt>ERROR</tt>, and which can
         * safely be ignored. In compatible shells, if colours are enabled in
         * the Session configuration, the colour of these messages is yellow.
         * The symbol of this logging level is <tt>'W'</tt>.
         */
        WARNING         = 0x20,

        /**
         * This logging level is used for signaling the occurence of runtime
         * exceptions that were successfully handled, so that the Session may
         * continue normally. In compatible shells, if colours are enabled in
         * the Session configuration, the colour of these messages is red. The
         * symbol of this logging level is <tt>'E'</tt>.
         */
        ERROR           = 0x40,

        /**
         * This logging level is used for signaling the occurence of fatal
         * runtime errors, that result in abnormal shutdown of the application.
         * In compatible shells, if colours are enabled in the Session
         * configuration, the colour of these messages is red. ADAPRO considers
         * every unhandled exception or error as fatal. The symbol of this
         * logging level is <tt>'F'</tt>.
         */
        FATAL           = 0x80
    };
}

namespace Library
{
    using namespace ADAPRO::Data;

    /**
     * A simple conversion from LoggingLevels to strings. Returns the first
     * letter in the name of the LoggingLevel in uppercase.
     *
     * @param severity  The logging level.
     * @return          A character symbolizing that level.
     * @throws std::domain error If applied to an invalid value.
     */
    template<> inline std::string symbol(const LoggingLevel severity)
    {
        switch (severity)
        {
            case ADAPRO_DEBUG:  return "A";
            case FSM:           return "M";
            case USER_DEBUG:    return "D";
            case INFO:          return "I";
            case SPECIAL:       return "S";
            case WARNING:       return "W";
            case ERROR:         return "E";
            case FATAL:         return "F";
            default:            throw std::domain_error("Invalid LoggingLevel.");
        }
    }

    /**
     * Converts the given string into a logging mask.
     *
     * @param input String representation of the logging mask.
     * @return      Integral representation of the logging mask.
     * @throws std::domain_error If applied to a malformed logging mask string.
     * @see ADAPRO::Data::LOGGING_MASK
     */
    inline uint8_t logging_mask(const std::string& input)
    {
        if (ADAPRO::Library::is_logging_mask(input))
        {
            uint8_t mask{0};
            for (const char x : input)
            {
                switch (x)
                {
                    case 'A': mask |= ADAPRO_DEBUG; break;
                    case 'M': mask |= FSM;          break;
                    case 'D': mask |= USER_DEBUG;   break;
                    case 'I': mask |= INFO;         break;
                    case 'S': mask |= SPECIAL;      break;
                    case 'W': mask |= WARNING;      break;
                    case 'E': mask |= ERROR;        break;
                    case 'F': mask |= FATAL;        break;
                }
            }
            return mask;
        }
        else
        {
            throw std::domain_error("Invalid logging mask.");
        }
    }

    /**
     * A simple conversion from <tt>std::string</tt> to
     * <tt>ADAPRO::Data::LoggingLevel</tt>. Used when reading the configuration
     * file. <em>Note that the string equivalent of <tt>ADAPRO_DEBUG</tt> is
     * <tt>"ADAPRO"</tt> and the string equivalent of <tt>USER_DEBUG</tt> is
     * <tt>"DEBUG"</tt>.</em>
     *
     * @param str   String
     * @return      Logging level
     * @throws std::domain_error If the string didn't represent a valid logging
     * level.
     */
    template<> inline LoggingLevel read(const std::string& input)
    {
             if (input == "ADAPRO")     {return ADAPRO_DEBUG;}
        else if (input == "FSM")        {return FSM;}
        else if (input == "DEBUG")      {return USER_DEBUG;}
        else if (input == "INFO")       {return INFO;}
        else if (input == "SPECIAL")    {return SPECIAL;}
        else if (input == "WARNING")    {return WARNING;}
        else if (input == "ERROR")      {return ERROR;}
        else if (input == "FATAL")      {return FATAL;}
        else
        {
            throw std::domain_error("\"" + input + "\" is not a valid logging"
                " level. (Is it all uppercase?)");
        }
    }
}
}

#endif /* ADAPRO_LOGGING_LEVEL_HPP */


/*
 * File:   State.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 July 2015, 17:52
 */

#ifndef ADAPRO_STATE_HPP
#define	ADAPRO_STATE_HPP

#include <stdint.h>
#include <string>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * Represents the possible states of a Thread. For more information on the
     * state semantics, please consult the Technical Documentation.
     *
     * @see ADAPRO::Control::Thread
     */
    enum State
    {
        /**
         * The initial state of a Thread. In this state, the Thread has not yet
         * begun to carry out its computational task.
         */
        READY       = 0x01,

        /**
         * In this transitional state, a Thread is preparing to execute its
         * computational task. During this state, Thread invokes
         * <tt>prepare</tt>.
         *
         * @see ADAPRO::Control::Thread::prepare
         */
        STARTING    = 0x02,

        /**
         * This is the state where the actual computational task of the Thread
         * is carried out. During this state, Thread repeatedly invokes
         * <tt>execute</tt>.
         *
         * @see ADAPRO::Control::Thread::execute
         */
        RUNNING     = 0x04,

        /**
         * In this state, the Thread has temporarily stopped its calculation. It
         * can still return to <tt>RUNNING</tt>, or move to <tt>STOPPING</tt>
         * depending on the command.
         */
        PAUSED      = 0x08,

        /**
         * In this transitional state, the Thread is performing some post-run
         * task. During this state, Thread invokes <tt>finish</tt>.
         *
         * @see ADAPRO::Control::Thread::finish
         */
        STOPPING    = 0x10,

        /**
         * This is the state when the computational task of the Thread has been
         * carried out and resources released. It should be safe to delete a
         * Thread in this state.
         */
        STOPPED     = 0x20,

        /**
         * In this transitional state, the execution of the Thread is being
         * ceased prematurely (due to an error or other exceptional
         * circumstance). During this state, Thread invokes
         * <tt>handle_exception</tt>.
         *
         * @see ADAPRO::Control::Thread::handle_exception
         */
        ABORTING    = 0x40,

        /**
         * This is the state when the computational task of the Thread has been
         * aborted and the computations have ceased. The reseources used by
         * Thread should have been released, so it should be safe to delete a
         * Thread in this state.
         */
        ABORTED     = 0x80

    };

    /**
     * A bit mask to be used when waiting a <tt>Thread</tt> to start.
     */
    constexpr uint8_t START_MASK =
        ADAPRO::Data::State::RUNNING    | ADAPRO::Data::State::PAUSED   |
        ADAPRO::Data::State::STOPPING   | ADAPRO::Data::State::STOPPED  |
        ADAPRO::Data::State::ABORTING   | ADAPRO::Data::State::ABORTED;

    /**
     * A bit mask to be used when waiting a <tt>Thread</tt> to pause.
     */
    constexpr uint8_t PAUSE_MASK =
        ADAPRO::Data::State::PAUSED     | ADAPRO::Data::State::STOPPING |
        ADAPRO::Data::State::STOPPED    | ADAPRO::Data::State::ABORTING |
        ADAPRO::Data::State::ABORTED;

    /**
     * To be used when waiting a <tt>Thread</tt> to change its state from
     * <tt>PAUSED</tt> to <tt>RUNNING</tt>, i.e. for implementing
     * <tt>resume_sync()</tt>.
     *
     * By the definition of <tt>ADAPRO::Data::State</tt>, it holds that
     * <tt>RESUME</tt> &lt; <tt>PAUSED</tt>. This implies that when resuming
     * a <tt>Thread</tt> in state <tt>PAUSED</tt> in a blocking manner, it
     * is not correct to wait for a state greater than or equal to
     * <tt>RUNNING</tt>. Instead, in this situation
     * <tt>wait_for_state_mask</tt> should be called with this state mask.
     * As a <tt>Thread</tt> cannot ever move backwards to states
     * <tt>STARTING</tt> or <tt>RUNNING</tt>, they are not included in this
     * mask. (This issue was found with the Spin model checker.)
     */
    constexpr uint8_t RESUME_MASK =
        ADAPRO::Data::State::RUNNING    | ADAPRO::Data::State::STOPPING |
        ADAPRO::Data::State::STOPPED    | ADAPRO::Data::State::ABORTING |
        ADAPRO::Data::State::ABORTED;

    /**
     * A bitmask of the states in which a Thread is considered to be halting or
     * halted.
     */
    constexpr uint8_t HALTING_MASK =
        ADAPRO::Data::State::STOPPING   | ADAPRO::Data::State::ABORTING |
        ADAPRO::Data::State::STOPPED    | ADAPRO::Data::State::ABORTED;

    /**
     * A bitmask of the end states, i.e. STOPPED and ABORTED.
     */
    constexpr uint8_t HALTED_MASK =
        ADAPRO::Data::State::STOPPED | ADAPRO::Data::State::ABORTED;

    /**
     * A bitmask of the end state ABORTED.
     */
    constexpr uint8_t ABORT_MASK = ADAPRO::Data::State::ABORTED;
}

namespace Library
{
    using namespace ADAPRO::Data;

    /**
     * Returns a three-letter abbreviated name of the state.
     *
     * @param state The state.
     * @return The abbreviated name of the state.
     * @throws std::domain_error If applied to an invalid value.
     */
    template<> inline std::string symbol(const State state)
    {
        switch (state)
        {
            case READY:    return "Rdy";
            case STARTING: return "Stg";
            case RUNNING:  return "Rng";
            case PAUSED:   return "Psd";
            case STOPPING: return "Spg";
            case STOPPED:  return "Spd";
            case ABORTING: return "Abg";
            case ABORTED:  return "Abd";
            default: throw std::domain_error("Invalid State.");
        }
    }

    /**
     * A straightforward conversion from <tt>ADAPRO::Data::State</tt> to
     * <tt>std::string</tt>. Each State is mapped to its unqualified name in
     * upper case.
     *
     * @param state A State.
     * @return      The corresponding string.
     * @throws std::domain_error If applied to an invalid value.
     */
    template<> inline std::string show(const State state)
    {
        switch (state)
        {
            case READY:    return "READY";
            case STARTING: return "STARTING";
            case RUNNING:  return "RUNNING";
            case PAUSED:   return "PAUSED";
            case STOPPING: return "STOPPING";
            case STOPPED:  return "STOPPED";
            case ABORTING: return "ABORTING";
            case ABORTED:  return "ABORTED";
            default: throw std::domain_error("Invalid State.");
        }
    }
}
}

#endif	/* ADAPRO_STATE_HPP */


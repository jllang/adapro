/*
 * File:   SystemdWrapper.hpp
 * Author: John Lång
 *
 * Created on 7 January 2019, 9:56
 */

#ifdef __linux__
#ifndef ADAPRO_SYSTEMD_WRAPPER_HPP
#define ADAPRO_SYSTEMD_WRAPPER_HPP

//#include <cerrno>
//#include <cstring>
#include <string>
#include <systemd/sd-daemon.h>
#include "../library/GenericFunctions.hpp"
#include "../data/State.hpp"

namespace ADAPRO
{
/**
 * A wrapper namespace for systemd integration. <em>The procedures defined
 * in this namespace are not meant to be used by the user application.</em>
 * Instead, on Linux systems, ADAPRO framework automatically takes care of
 * transmitting <tt>Supervisor</tt> state and notifying Systemd watchdog when
 * running in daemon mode.
 *
 * @see ADAPRO::Control::Session
 * @see ADAPRO::Control::Supervisor
 */
namespace Systemd
{

    /**
     * A wrapper method for notifying systemd in a mutually excluded fashion.
     * Ignores all errors. Currently, calls to this routine are not mutually
     * excluded. <em>Not meant to be invoked by user code.</em>
     *
     * @param message   The message to be passed on to <tt>sd_notify</tt>.
     */
    inline void notify(const std::string& message) noexcept
    {
//        errno = sd_notify(0, message.c_str());
//        if (errno != 0)
//        {
//            throw std::runtime_error
//            (
//                    "sd_notify failed (errno " + std::to_string(errno) +
//                    "):\n    \"" + std::string(std::strerror(errno)) + "\""
//            );
//        }
        sd_notify(0, message.c_str()); // TODO: Introduce a guard mechanism enabling this only for daemon mode
    }

    /**
     * Informs the operating system about a state transition of the ADAPRO
     * process. Currently, this operation is only supported on Linux systems
     * using <tt>systemd</tt>. This method is only used when running in
     * <emph>daemon mode</emph>. The purpose of this method is to make
     * debugging and diagnostics easier. <em>Not meant to be invoked by user
     * code.</em>
     *
     * @param s The new state of the ADAPRO process.
     */
    inline void set_process_state(const ADAPRO::Data::State s)
    noexcept
    {
        notify("STATUS=" + ADAPRO::Library::show<ADAPRO::Data::State>(s));
    }

    /**
     * On a Linux platform with <tt>systemd</tt> support, when running the
     * ADAPRO application in daemon mode, this procedure enables the
     * <tt>systemd</tt> watchdog mechanism. <em>Not meant to be invoked by user
     * code.</em>
     */
    inline void enable_watchdog() noexcept
    {
        notify("WATCHDOG_USEC=2000000");
    }

    /**
     * On a Linux platform with <tt>systemd</tt> support, when running the
     * ADAPRO application in daemon mode, this procedure disables the
     * <tt>systemd</tt> watchdog mechanism. <em>Not meant to be invoked by user
     * code.</em>
     */
    inline void disable_watchdog() noexcept
    {
        notify("WATCHDOG_USEC=0");
    }

    /**
     * On a Linux platform with <tt>systemd</tt> support, when running the
     * ADAPRO application in daemon mode, this procedure notifies the
     * <tt>systemd</tt> watchdog. <em>Not meant to be invoked by user code.</em>
     */
    inline void notify_watchdog() noexcept
    {
        notify("WATCHDOG=1");
    }
}
}

#endif /* ADAPRO_SYSTEMD_WRAPPER_HPP */
#endif /* __linux__ */
/*
 * File:   Supervisor.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 9:30
 */

#ifndef ADAPRO_SUPERVISOR_HPP
#define ADAPRO_SUPERVISOR_HPP

#include <cstdint>
#include <string>
#include <map>
#include <list>
#include <memory>
#include <functional>
#include <mutex>
#include "../data/Typedefs.hpp"
#ifndef EXCLUDE_DIM
#include "../DIM/Typedefs.hpp"
#endif
#include "Thread.hpp"
#include "Worker.hpp"

class SupervisorTest;
class FBIAgentTest;
class ProducerConsumerTest;
class ControllerTest;

namespace ADAPRO
{
namespace Data
{
    enum Command;
}
namespace Control
{
#ifndef VERIFY
    class Logger;
#endif
    class Session;

    /**
     * <p><tt>Supervisor</tt> is a <tt>Worker</tt> whose responsibility is to
     * manage other <tt>Worker</tt>s. The state transitions of
     * <tt>Supervisor</tt> are propagated to all <tt>Worker</tt>s under its
     * management. On Linux systems, when running <em>daemon mode</em>,
     * <tt>Supervisor</tt> is also responsible for informing Systemd about its
     * state changes and notifying the watchdog on regular intervals. When
     * running on <em>DIM server mode </em>, <tt>Supervisor</tt> also keeps
     * informing the DIM server about its state transitions and propagates
     * commands received through a DIM service to <tt>Worker</tt>s.</p>
     * <em>The framework user shouldn't create a <tt>Supervisor</tt> directly,
     * but use static methods <tt>Session::INITIALIZE</tt> and
     * <tt>Session::RUN</tt> instead.</em> ADAPRO assumes that there are no
     * other <tt>Supervisor</tt> instances running concurrently, than the one
     * owned by the active <tt>Session</tt> instance.</p>
     *
     * @see ADAPRO::Control::Thread
     * @see ADAPRO::Control::Worker
     * @see ADAPRO::Control::Session
     */
    class Supervisor final: public Worker
    {
        friend Session;
        friend SupervisorTest;
        friend FBIAgentTest;
        friend ProducerConsumerTest;
        friend ControllerTest;

        /**
         * <tt>true</tt> if and only if the <tt>Supervisor</tt> has been started
         * (by calling <tt>ADAPRO::Thread::start</tt> on it).
         *
         * @see ADAPRO::Control::Thread::start
         */
        bool started;

        /**
         * <tt>true</tt> if and only if every <tt>Worker</tt> has stopped (or
         * when this is being checked).
         *
         * @see ADAPRO::Data::State
         */
        bool all_workers_stopped;

        /**
         * <tt>true</tt> if and only if one or more <tt>Worker</tt>s have moved
         * to state <tt>ABORTED</tt>.
         *
         * @see ADAPRO::Data::State
         */
        bool some_workers_dead;

#ifdef __linux__
        /**
         * <tt>true</tt> if and only if the ADAPRO Session is running in
         * <em>daemon mode</em>, in which case <tt>Supervisor</tt> keeps
         * notifying the Systemd watchdog on regular basis, on Linux systems.
         */
        bool daemon_mode_enabled;
#endif

        /**
         * List of the managed <tt>Worker</tt> <tt>Threads</tt>.
         */
        std::list<std::unique_ptr<ADAPRO::Control::Worker>> workers;

        /**
         * This mutex object is used for ensuring mutual exclusion to
         * propagating commands to <tt>Worker</tt>s.
         */
        std::mutex command_mutex;

        /**
         * Sends a command to the given <tt>Worker</tt>.
         *
         * @param worker_ptr    The recipient of the command.
         * @param command       The command to be sent.
         * @param wait          If <tt>true</tt> this method will block until
         * the <tt>Worker</tt> has performed an appropriate state transition.
         */
        void send_command
        (
                unique_ptr<Worker>& worker_ptr,
                const ADAPRO::Data::Command command,
                const bool wait
        )
        noexcept;

        /**
         * Gives the <tt>Worker</tt> 10 seconds of time to stop gracefully. If
         * the <tt>Worker</tt> fails to shut down gracefully within this period,
         * then it will be stopped forcefully.
         *
         * @param worker_ptr    Pointer to the <tt>Worker</tt>.
         */
        void wait_or_kill(std::unique_ptr<ADAPRO::Control::Worker>& worker_ptr)
        noexcept;

        /**
         * Chooses the appropriate command propagation mode according to the
         * Session configuration, and propagates the given command to all
         * <tt>Worker</tt>s.
         *
         * @param command           The command to be propagated.
         * @see ADAPRO::Data::SERIALIZE_COMMANDS
         */
        void propagate_command
        (
                const ADAPRO::Data::Command command
        )
        noexcept;

        /**
         * Propagates the given command to all <tt>Worker</tt>s and blocks until
         * they have moved to a state greater than or equal to the given
         * expected state. This procedure only blocks after sending the
         * commands.
         *
         * @param command The command to be given to every <tt>Worker</tt>.
         */
        void propagate_command_in_parallel
        (
                const ADAPRO::Data::Command command
        )
        noexcept;

        /**
         * Propagates the given command serially to the <tt>Worker</tt>s in
         * <em>Last In, First Out</em> (LIFO) mode. This means that the
         * <tt>Worker</tt>, whose factory is first in the list given in the
         * <tt>Supervisor</tt>'s constructor, will be started/resumed first and
         * stopped/paused last. This procedure blocks between after sending a
         * command to each <tt>Worker</tt>, until the <tt>Worker</tt> has moved
         * to a state greater than or equal to the given expected state.
         *
         * @param command The command to be given to every <tt>Worker</tt>.
         */
        void propagate_command_in_lifo
        (
                const ADAPRO::Data::Command command
        )
        noexcept;

        /**
         * Propagates the given command to the <tt>Worker</tt>s in increasing
         * order by index.
         *
         * @param command   The command to propagate.
         * @param serialize The <tt>Supervisor</tt> will wait for every
         * <tt>Worker</tt> to perform their state transition after sending the
         * command, if and only if this value is set to <tt>true</tt>.
         */
        void covariant_propagation(const ADAPRO::Data::Command command,
                const bool serialize) noexcept;

        /**
         * Propagates the given command in decreasing (i.e. reverse) order by
         * index. The <tt>Supervisor</tt> will wait for every <tt>Worker</tt> to
         * perform their state transition after sending the command.
         *
         * @param command   The command to propagate.
         */
        void contravariant_propagation(const ADAPRO::Data::Command command)
        noexcept;

    protected:

        /**
         * During this procedure, <tt>Supervisor</tt> constructs the
         * <tt>Worker</tt>s by calling the factory functions given as
         * constructor parameters. After the <tt>Worker</tt>s are created,
         * <tt>Supervisor</tt> starts them and moves to <tt>RUNNING</tt>.
         * Whether or not <tt>Supervisor</tt> waits for <tt>Worker</tt>s to
         * perform appropriate state transitions, depends on the <em>command
         * propagation mode</em>.
         *
         * @see ADAPRO::Data::Paramater::SERIALIZE_COMMANDS
         */
        virtual void prepare() override;

        /**
         * During this procedure, <tt>Supervisor</tt> inspects the states of the
         * <tt>Worker</tt>s. If all <tt>Worker</tt>s have stopped or one or more
         * of them have aborted, then <tt>Supervisor</tt> initiates a transition
         * to the state <tt>STOPPING</tt>.
         *
         * @see ADAPRO::Data::State
         */
        virtual void execute() override;

        /**
         * As of ADAPRO 5.0.0, this virtual method of <tt>Supervisor</tt> does
         * nothing.
         */
        virtual void finish() override;

    public:

        /**
         * The deleted default constructor.
         */
        Supervisor() = delete;

        /**
         * Constructs a <tt>Supervisor</tt>. <em>NB: The arguments related to
         * <em>DIM server mode</em> require ADAPRO to be built with DIM support
         * and the configuration parameter <tt>DIM_SERVER_ENABLED</tt> to have
         * value <tt>TRUE</tt>. Otherwise, they have no effect.</em>
         *
         * @param logger                <tt>Logger</tt> of the ADAPRO Session.
         * @param configuration         The configuration to be passed on to the
         * @param worker_factories      List of the factory functions providing
         * unique pointers to <tt>Worker</tt>s.
         * @param dim_command_factories List of the factory functions providing
         * user-defined DIM commands. <em>Requires DIM server mode.</em>
         * @param dim_client_error_handler The DIM client error handler. <em>
         * Requires DIM server mode.</em>
         * @param dim_server_error_handler The DIM server error handler. <em>
         * Requires DIM server mode.</em>
         * @param state_ptr             Pointer to the memory address used for
         * storing the state of this <tt>Supervisor</tt>. <em>Requires DIM
         * server mode.</em>
         *
         * @throws std::out_of_range    If the configuration key
         * <tt>ADAPRO::Data::Parameter::SUPERVISOR_CORE</tt> was undefined.
         * @see ADAPRO::Data::Parameter::DAEMON_ENABLED
         * @see ADAPRO::Data::Parameter::DIM_SERVER_ENABLED
         * @see ADAPRO::Data::Parameter::SUPERVISOR_CORE
         * @see ADAPRO::Data::worker_factory_t
         * @see ADAPRO::DIM::command_factory_t
         */
        Supervisor
        (
#ifndef VERIFY
                Logger& logger,
#endif
                const ADAPRO::Data::config_t& configuration
                , std::list<ADAPRO::Data::worker_factory_t>&& worker_factories
#ifndef EXCLUDE_DIM
                , std::list<ADAPRO::DIM::command_factory_t>&& dim_command_factories
                , ADAPRO::DIM::error_callback_t dim_client_error_handler
                , ADAPRO::DIM::error_callback_t dim_server_error_handler
                , uint32_t* state_ptr
#endif
        )
        noexcept;

        /**
         * A debugging command used for printing the states of the
         * <tt>Worker</tt>s managed by the <tt>Supervisor</tt>.
         */
        virtual string report() noexcept override;

        /**
         * The virtual destructor of <tt>Supervisor</tt>. Stops and joins the
         * <tt>Worker</tt>s, if not done already.
         */
        virtual ~Supervisor() noexcept;
    };
}
}

#endif /* ADAPRO_SUPERVISOR_HPP */


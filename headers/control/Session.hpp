/*
 * File:   Session.hpp
 * Author: John Lång
 *
 * Created on 4 January 2019, 14:51
 */

#ifndef ADAPRO_SESSION_HPP
#define ADAPRO_SESSION_HPP

#include <cstdint>
#include <atomic>
#include <string>
#include <map>
#ifndef VERIFY
#include "Logger.hpp"
#endif
#include "Supervisor.hpp"
#include "../data/Typedefs.hpp"

using namespace std;

namespace ADAPRO
{
namespace Data
{
    struct Context;
    enum State;
}

namespace Control
{

    /**
     * <p>
     * A singleton (one-instance) class that does all the other bookkeeping than
     * managing <em>Worker Threads</em> and DIM services in a black box fashion.
     * This bookkeeping includes:</p>
     * <ol>
     *     <li>Registering signal and terminate handlers that may call
     *         user-defined signal callbacks.</li>
     *     <li>Obtaining, parsing and checking configuration entries possibly
     *         provided in command line arguments and/or configuration file;</li>
     *     <li>Creating the <tt>Supervisor</tt> that manages <tt>Worker</tt>s
     *         possibly including a (DIM) <tt>Controller</tt> that manages DIM
     *         services published by the ADAPRO framework and possibly also by
     *         the user; and</li>
     *     <li>On Linux systems, when running daemon mode, communicating with
     *         Systemd.</li>
     * </ol>
     * <p>The user application should first call <tt>Session::INITIALIZE</tt>
     * and then <tt>Session::RUN</tt> to run an ADAPRO Session.</p>
     *
     * @see ADAPRO::Control::Supervisor
     * @see ADAPRO::Control::Worker
     * @see ADAPRO::Control::Thread
     * @see ADAPRO::DIM::Controller
     */
    class Session // This class could actually be a Thread as well!
    {
        /**
         * The final configuration for the ADAPRO Session that contains the
         * entries from the initial configuration and possible updates read from
         * a configuration file.
         *
         * @see ADAPRO::Library::import_configuration
         * @see ADAPRO::Data::Context::configuration_paths
         * @see ADAPRO::Control::Session
         */
        const ADAPRO::Data::config_t configuration;

        /**
         * The status code fo the ADAPRO Session.
         */
        atomic<uint8_t> status_code;

#ifndef EXCLUDE_DIM

        /**
         * A redundant numeric representation of the state of the Supervisor,
         * used for publishing a DIM service.
         */
        uint32_t supervisor_state;
#endif

#ifndef VERIFY
        /**
         * The Logger instance of the ADAPRO Session.
         */
        ADAPRO::Control::Logger logger;
#endif

        /**
         * The Supervisor Thread of the Session. Supervisor is responsible for
         * propagating commands to the user-defined Worker Threads. When Session
         * executes its method <tt>run</tt>, it invokes <tt>start</tt> on
         * Supervisor.
         *
         * @see ADAPRO::Control::Supervisor
         */
        ADAPRO::Control::Supervisor supervisor;

        /**
         * A constructor for Session. <em>NB: The arguments related to DIM
         * server mode require ADAPRO to be built with DIM support and the
         * configuration parameter <tt>DIM_SERVER_ENABLED</tt> to have value
         * <tt>TRUE</tt>. Otherwise, they have no effect.</em>
         *
         * @param configuration     The final configuration to be moved into the
         * Session under construction.
         * @param logger            The final logger to be moved into the
         * Session under construction.
         * @param worker_factories  The list of worker factories to be copied
         * into the Session under construction.
         * @param dim_command_factories List of the factory functions providing
         * user-defined DIM commands. <em>Requires DIM server mode.</em>
         * @param dim_client_error_handler The DIM client error handler. <em>
         * Requires DIM server mode.</em>
         * @param dim_server_error_handler The DIM server error handler. <em>
         * Requires DIM server mode.</em>
         * @param state_ptr             Pointer to the memory address used for
         * storing the state of this Supervisor. <em>Requires DIM server mode.
         * </em>
         */
        Session
        (
                ADAPRO::Data::config_t&& configuration
#ifndef VERIFY
                , ADAPRO::Control::Logger&& logger
#endif
                , std::list<ADAPRO::Data::worker_factory_t>&& worker_factories
#ifndef EXCLUDE_DIM
                , std::list<ADAPRO::DIM::command_factory_t>&& dim_command_factories
                , ADAPRO::DIM::error_callback_t dim_client_error_handler
                , ADAPRO::DIM::error_callback_t dim_server_error_handler
#endif
        )
        noexcept;

#ifdef __divine__
        /**
         * A callback that will be invoked after Session has been initialized.
         */
        std::function<void(Supervisor& supervisor)> environment_callback;
#endif

        ~Session() noexcept {}

        /**
         * <p>Gets the current status code, which is a bit mask of the following
         * flags: </p>
         * <table>
         *     <thead>
         *         <tr><th>Flag</th><th>Decimal</th><th>Description</th></tr>
         *     </thead>
         *     <tbody>
         *         <tr><td><tt>FLAG_BAD_CONFIG</tt></td><td>1</td><td>Malformed configuration file</td></tr>
         *         <tr><td><tt>FLAG_UNHANDLED_EXCEPTION</tt></td><td>2</td><td>Generic runtime error</td></tr>
         *         <tr><td><tt>FLAG_SEGV</tt></td><td>4</td><td>Segmentation violation</td></tr>
         *         <tr><td><tt>FLAG_KNOWN_SIGNAL</tt></td><td>8</td><td>Supported signal</td></tr>
         *         <tr><td><tt>FLAG_UNKNOWN_SIGNAL</tt></td><td>16</td><td>Unsupported signal</td></tr>
         *         <tr><td><tt>FLAG_DIM_ERROR</tt></td><td>32</td><td>DIM error</td></tr>
         *         <tr><td><tt>FLAG_TIMEOUT</tt></td><td>64</td><td>Shutdown timed out</td></tr>
         *         <tr><td><tt>FLAG_WORKER</tt></td><td>128</td><td>One or more workers have died</td></tr>
         *     </tbody>
         * </table>
         * <p>This function is called by <tt>ADAPRO::Control::main</tt> when
         * the application is about to halt, and it is used for calculating the
         * process exit code.</p>
         *
         * @return The status code.
         * @see ADAPRO::Data::FLAG_BAD_CONFIG
         * @see ADAPRO::Data::FLAG_UNHANDLED_EXCEPTION
         * @see ADAPRO::Data::FLAG_SEGV
         * @see ADAPRO::Data::FLAG_KNOWN_SIGNAL
         * @see ADAPRO::Data::FLAG_UNKNOWN_SIGNAL
         * @see ADAPRO::Data::FLAG_DIM_ERROR
         * @see ADAPRO::Data::FLAG_TIMEOUT
         * @see ADAPRO::Data::FLAG_WORKER
         * @see ADAPRO::Data::FLAG_USER
         */
        inline uint8_t get_status_code() noexcept
        {
            return status_code.load(std::memory_order_consume);
        }

        /**
         * Masks the given flags into the status code.
         *
         * @param flags The flags to be (<tt>OR</tt>) masked.
         */
        inline void mask_status_code(const uint8_t flags) noexcept
        {
            status_code.store
            (
                    status_code.load(std::memory_order_consume) | flags,
                    memory_order_release
            );
        }

        /**
         * Analogously to <tt>Thread::prepare</tt>, performs preparations that
         * take place during <tt>Session</tt> startup sequence.
         *
         * @throws an exception inheriting <tt>std::runtime_error</tt> to
         * indicate that the <tt>Session</tt> needs to abort. The exception will
         * be thrown if an unhandled exception or signal occurs elsewhere. Does
         * not generate exceptions or signals otherwise.
         */
        void prepare();

        /**
         * Analaogously to <tt>Thread::prepare</tt>, performs the actions
         * associated with a running <tt>Session</tt>.
         *
         * @throws an exception inheriting <tt>std::runtime_error</tt> to
         * indicate that the <tt>Session</tt> needs to abort. The exception will
         * be thrown if an unhandled exception or signal occurs elsewhere. Does
         * not generate exceptions or signals otherwise.
         */
        void execute();

        /**
         * Analogously to <tt>Thread::finish</tt>, performs the actions that are
         * needed before a started <tt>Session</tt> can be safely deleted.
         */
        void finish() noexcept;

    public:

        /**
         * The deleted default constructor.
         */
        Session() = delete;

        /**
         * Gets the state of the <tt>Supervisor</tt> of the <tt>Session</tt>.
         * Before <tt>Supervisor</tt> has started, the return value will always
         * be <tt>READY</tt>.
         *
         * @return A state.
         */
        static ADAPRO::Data::State GET_SUPERVISOR_STATE() noexcept;

        /**
         * Propagates the <tt>PAUSE</tt> command to all ADAPRO <tt>Worker</tt>s
         * through the <tt>Session</tt>'s <tt>Supervisor</tt>. Returns
         * immediately without blocking. <em>This method is not meant to be used
         * directly by the user application.</em>
         */
        static void PAUSE_ASYNC() noexcept;

        /**
         * Propagates the <tt>CONTINUE</tt> command to all ADAPRO <tt>Worker</tt>s
         * through the <tt>Session</tt>'s <tt>Supervisor</tt>. Returns
         * immediately without blocking. <em>This method is not meant to be used
         * directly by the user application.</em>
         */
        static void RESUME_ASYNC() noexcept;

        /**
         * Propagates the <tt>STOP</tt> command to all ADAPRO <tt>Worker</tt>s
         * through the <tt>Session</tt>'s <tt>Supervisor</tt>. Returns
         * immediately without blocking. <em>This method is not meant to be used
         * directly by the user application.</em>
         */
        static void STOP_ASYNC() noexcept;

        /**
         * Initiates an abnormal shutdown sequence. This static method should be
         * called if an only if a fatal runtime error has occured and the ADAPRO
         * Session has to be shut down as soon as possible.
         *
         * @param flags Status flags to be masked into the return value of the
         * ADAPRO Session.
         */
        static void ABORT_ASYNC(const uint8_t flags) noexcept;

        /**
         * Like <tt>ABORT_ASYNC</tt>, but blocks until the <tt>Session</tt>
         * instance has aborted.
         *
         * @param flags Status flags to be masked into the return value of the
         * ADAPRO Session.
         */
        static void ABORT_SYNC(const uint8_t flags) noexcept;

        /**
         * Checks if an abnormal shutdown sequence is in progress.
         *
         * @return <tt>true</tt> if and only if an abnormal shutdown sequence is
         * in progress.
         */
        static bool IS_ABORTING() noexcept;

        /**
         * Prints the given message using the Logger of the Session, if
         * available. If not, the message will be printed to <tt>stderr</tt>
         * without a timestamp, colouring, or a logging mask check.
         *
         * @param message   The message to be printed.
         * @param severity  Logging level. Has only effect if the Logger
         * singleton is available.
         */
        static void PRINT
        (
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity
        )
        noexcept;

        /**
         * Registers signal handler, processes configuration, creates a
         * <tt>Logger</tt> and a <tt>Session</tt>, based on the given
         * initialization context. <em>This method is not thread safe.</em>
         *
         * @param context   The initialization context.
         * @throws
         */
        static void INITIALIZE(ADAPRO::Data::Context&& context);

        /**
         * Runs the <tt>Session</tt> and returns a status code if the
         * <tt>Session</tt> halted. <em>This method is not thread safe.</em>
         *
         * @return The status code.
         * @see ADAPRO::Control::Session::get_status_code
         */
        static uint8_t RUN() noexcept;
    };
}
}

#endif /* ADAPRO_SESSION_HPP */
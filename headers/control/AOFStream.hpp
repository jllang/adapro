/*
 * File:   AOFStream.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 September 2016, 17:51
 */
#ifndef ADAPRO_AOFSTREAM_HPP
#define ADAPRO_AOFSTREAM_HPP

#include <cstdint>
#include <mutex>
#include <queue>
#include <fstream>
#include <string>
#include "Thread.hpp"
#include "AOStream.hpp"

using namespace ADAPRO::Data;
using namespace ADAPRO::Library;

namespace ADAPRO
{
namespace Control
{
#ifndef VERIFY
    class Logger;
#endif

    /**
     * AOFStream (Asynchronous Output File Stream) dumps <tt>T</tt>s
     * asynchronously into an output file. It is the reference implementation
     * for <tt>ADAPRO::Control::AOStream</tt>.
     *
     * @param T A type that must implement put to (&lt;&lt;) operator.
     * @see ADAPRO::Control::AOStream
     */
    template<typename T>
    class AOFStream final : public AOStream<T>
    {
        /**
         * A serial number used for generating unique names for AOFStream
         * instances.
         */
        static uint32_t S_NO;

        /**
         * Generates a unique name for a AOFStream instance.
         * @param type  Name of the type T. Used for printing more helpful
         * debug/error messages.
         * @return      The name for a AOFStream Thread.
         */
        static std::string GENERATE_NAME(const std::string&& type)
        {
            // TODO: Discard the parameter in favour of typeid(T).name().
            return "AOFStream<" + type + ">-" + to_string(S_NO++);
        }

    public:
        /**
         * The filename that was given to AOFStream in constructor.
         */
        const std::string filename;

    private:
        /**
         * The file output stream used for dumping T.
         */
        std::ofstream output;

    protected:
        virtual void prepare() override {}

        virtual void execute() override
        {
            // TODO: Try to implement the queue access in a lock-free manner.
            // I wonder why I need to explicitly use <tt>this</tt> pointer...
            std::unique_lock<std::mutex> lock(this->aostream_mutex);
            if (!this->queue.empty())
            {
                const T object = this->queue.front();
                this->queue.pop();
                lock.unlock();
                this->output << object << "\n";
            }
        }

        virtual void finish() override
        {
            while (!this->queue.empty())
            {
                const T object = this->queue.front();
                this->queue.pop();
                this->output << object << "\n";
            }
            this->print("Closing file \"" + this->filename + "\"...", ADAPRO_DEBUG);
            this->output.close();
        }
    public:

        /**
         * The default constructor for AOFStream.
         */
        AOFStream() = delete;

        /**
         * Constucts a AOFStream with the given type and output file.
         *
         * @param logger    The Logger instance of the ADAPRO Session.
         * @param type      Name of the type T. Used for printing more helpful
         * debug/error messages. For instance, when constructing an
         * <tt>AOFStream&lt;int&gt;</tt>, this parameter should be "int".
         * @param filename  Name of the output file.
         * @param preferred_core    Preferred CPU core number. A negative value
         * indicates no preference. For more information, see
         * <tt>ADAPRO::Control::Thread</tt>.
         * @see ADAPRO::Control::Thread
         */
        AOFStream
        (
#ifndef VERIFY
                Logger& logger,
#endif
                std::string&& type,
                std::string&& filename,
                const int preferred_core = -1
        )
        noexcept:
                AOStream<T>
                (
#ifndef VERIFY
                        logger,
#endif
                        GENERATE_NAME(std::move(type)),
                        preferred_core
                ),
                filename(std::move(filename)),
                output(this->filename)
        {
            Thread::start_sync();
        }

        ~AOFStream() noexcept
        {
            this->close();
        }
    };

    template<typename T> uint32_t AOFStream<T>::S_NO = 0;
}
}

#endif /* ADAPRO_AOFSTREAM_HPP */


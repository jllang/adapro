/*
 * File:   Either.hpp
 * Author: John Lång
 *
 * Created on 2 January 2019, 10:23
 */

#ifndef ADAPRO_EITHER_HPP
#define ADAPRO_EITHER_HPP

#include <functional>
#include <string>
#include <cstring>
#include <stdexcept>
#include <cerrno>
#include "../control/Session.hpp"
#include "../data/LoggingLevel.hpp"
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
namespace Control
{

/**
 * @param type The type of the exception to be caught.
 */
#define either_exception_handler(type)                                         \
    catch (const type& e)                                                      \
    {                                                                          \
        message = #type":\n    \"" + std::string(e.what()) + "\"";             \
    }

/**
 * A macro used for catching unidentified thrown objects in
 * <tt>Either::lift</tt>.
 */
#define either_default_exception_handler                                       \
    catch (...)                                                                \
    {                                                                          \
        message = std::string("Unidentified thrown object");                   \
    }

/**
 * The predicate that evaluates to <tt>true</tt> on all values.
 */
template<typename V> const ADAPRO::Data::predicate_t<V> TOP
(
        [](const V& v){return true;}
);

/**
 * The predicate that evaluates to <tt>false</tt> on all values.
 */
template<typename V> const ADAPRO::Data::predicate_t<V> BOTTOM
(
        [](const V& v){return false;}
);

/**
 * <p>This class was inspired by the Either Monad in Haskell. It can be used for
 * automating exception handling in a canonical way. An <tt>Either</tt> instance
 * represents the result of a computation that can be either successful or
 * failed. For the successful case, the result is contained in the field
 * (member) <tt>right</tt>. For the failure case, an error message is contained
 * in the field (member) <tt>left</tt>. The field <tt>is_right</tt> will contain
 * the value <tt>true</tt> if and only if the computation was successful.</p>
 * <p>The Haskell operator <tt>(>>=)</tt> is implemented as the method
 * <tt>bind</tt>.
 * </p>
 *
 * @param T The type of the argument of the computational action. An
 * <tt>Either</tt> instance contains a <tt>T</tt> value.
 * @param U The type of the return value of the computational action that will
 * be stored in an <tt>Either</tt> object after performing a <tt>lift</tt> or
 * <tt>then</tt> operation. Must have a default constructor.
 * @see ADAPRO::Library::Either::lift
 * @see ADAPRO::Library::Either::then
 */
template<typename T, typename U> struct Either
{
    /**
     * <tt>true</tt> if and only if this Either instance contains a <tt>U</tt>
     * value obtained from a successfully evaluated action.
     */
    bool is_right;

    /**
     * If the computation that created this <tt>Either</tt> instance failed,
     * then this field (member) may contain an error message. Otherwise, this
     * field (member) contains a default constructed value.
     */
    std::string left;

    /**
     * If the computation that created this <tt>Either</tt> instance failed,
     * then this field (member) contains the result. Otherwise, this field
     * (member) contains a default constructed value.
     */
    T right;

    /**
     * The default constructor for <tt>Either</tt>. It is considered to be
     * vacuously successful.
     */
    Either() noexcept: is_right(true), left(), right() {}

    /**
     * A constructor for Either. Please consider using <tt>make_left</tt> or
     * <tt>make_right</tt> instead.
     *
     * @param is_right  Should be <tt>true</tt> if an only if the object under
     * construction represents the result of a successful computation.
     * @param left      If <tt>is_right</tt> is <tt>false</tt>, then this value
     * should be an appropriate error message. Otherwise, this value should be
     * an empty string.
     * @param right     If <tt>is_right</tt> is <tt>true</tt>, then this value
     * should be a (legal) object of type <tt>T</tt>. Otherwise, this value
     * should be default constructed.
     *
     * @see ADAPRO::Library::Either::make_left
     * @see ADAPRO::Library::Either::make_right
     */
    Either(const bool is_right, const std::string& left, const T& right)
    noexcept:
            is_right(is_right),
            left(left),
            right(right)
    {}

    /**
     * The deleted copy constructor. Use move constructor instead.
     *
     * @param other The <tt>Either</tt> instance to be copied.
     */
    Either(const Either& other) = delete;

    /**
     * The move constructor.
     *
     * @param other The <tt>Either</tt> instance to be moved.
     */
    Either(Either&& other) noexcept:
            is_right(other.is_right),
            left(std::move(other.left)),
            right(std::forward<T>(other.right))
    {}

    /**
     * The deleted copy assignment operator. Use move assingment instead.
     *
     * @param other The <tt>Either</tt> instance to be copied.
     * @return      A reference to the copy of the given object.
     */
    inline Either& operator =(const Either& other) = delete;

    /**
     * A static factory method that creates a new <tt>Either</tt> instance
     * representing a faied computation.
     *
     * @param message   An error message.
     * @return          The corresponding <tt>Either</tt> instance.
     */
    static inline Either make_left(const std::string& message) noexcept
    {
        return Either(false, message, T());
    }

    /**
     * A static factory method that creates a new <tt>Either</tt> instance
     * representing a successful computation.
     *
     * @param value The result of the computation.
     * @return      The corresponding <tt>Either</tt> instance.
     */
    static inline Either make_right(const T& value) noexcept
    {
        return Either(true, std::string(), value);
    }

    /**
     * Lifts a function from <tt>T</tt> to <tt>U</tt> into a function from
     * <tt>T</tt> to either <tt>std::string</tt> (in case of failure) or
     * <tt>U</tt> (in case of success), and applies it to a value of type
     * <tt>T</tt>. Mathematically, this static method represents an uncurried
     * functor from category <em>D</em> (of C++ programs) to its Kleisli
     * category <em>D_T</em>. In addition to exceptions, error numbers,
     * precondition and postcondition violations, this method also guards
     * against abnormal shutdown of the ADAPRO Session by aborting evaluation.
     *
     * @param action        A (possibly impure) function from <tt>T</tt> to
     * <tt>U</tt> that may fail. The function fails if and only if it throws an
     * exception or sets <tt>errno</tt> to a nonzero value.
     * @param argument      An argument for the given function.
     * @param precondition  A precondition of the action that can be used for
     * performing a sanity check on its input. Every value of type
     * <tt>T</tt> must satisfy this. The default value is
     * <tt>ADAPRO::Library::TOP</tt>, a predicate that is true for all values.
     * @param postcondition  A postcondition of the action that can be used for
     * performing a sanity check on its output. Every value of type
     * <tt>U</tt> must satisfy this. The default value is
     * <tt>ADAPRO::Library::TOP</tt>, a predicate that is true for all values.
     * @return              The lifted return value.
     * @see ADAPRO::Library::TOP
     * @see ADAPRO::Library::Either::bind
     */
    static Either lift
    (
            const ADAPRO::Data::action_t<T, U>& action,
            const T& argument
#ifdef DEBUG
            , const ADAPRO::Data::predicate_t<T>& precondition = TOP<T>
            , const ADAPRO::Data::predicate_t<U>& postcondition = TOP<U>
#endif
    )
    noexcept
    {
        bool success(false);
        std::string message;
        U payload;
        try
        {
            if (ADAPRO::Control::Session::IS_ABORTING())
            {
                throw std::runtime_error("Evaluation cancelled due to abnormal shutdown.");
            }
#ifdef DEBUG
            if (!precondition(argument))
            {
                throw std::logic_error("Precondition failure.");
            }
#endif
            errno = 0;
            payload = action(argument);
            if (errno != 0)
            {
                throw std::runtime_error
                (
                        "errno " + std::to_string(errno) + ": " +
                        std::string(std::strerror(errno))
                );
            }
#ifdef DEBUG
            if (!postcondition(payload))
            {
                throw std::logic_error("Postcondition failure.");
            }
#endif
            success = true;
        }
        either_exception_handler(std::ios_base::failure)
        either_exception_handler(std::system_error)
        either_exception_handler(std::regex_error)
        either_exception_handler(std::underflow_error)
        either_exception_handler(std::overflow_error)
        either_exception_handler(std::range_error)
        either_exception_handler(std::runtime_error)
        either_exception_handler(std::out_of_range)
        either_exception_handler(std::length_error)
        either_exception_handler(std::domain_error)
        either_exception_handler(std::invalid_argument)
        either_exception_handler(std::logic_error)
        either_exception_handler(std::bad_weak_ptr)
        either_exception_handler(std::bad_typeid)
        either_exception_handler(std::bad_function_call)
        either_exception_handler(std::bad_exception)
        either_exception_handler(std::bad_cast)
        either_exception_handler(std::bad_array_new_length)
        either_exception_handler(std::bad_alloc)
        either_exception_handler(std::exception)
        either_default_exception_handler
        if (!success)
        {
            ADAPRO::Control::Session::PRINT
            (
                "Monad evaluation failure:\n    " + message,
                ADAPRO::Data::LoggingLevel::ADAPRO_DEBUG
            );
        }
        return Either(success, std::move(message), std::forward<U>(payload));
    }

    /**
     * Binds the value of this <tt>Either</tt> instance to the given action, if
     * and only if this instance represents a value obtained from a successful
     * computation. Otherwise, the action will be skipped. Inspired by the
     * Haskell operator <tt>(>>=)</tt>.
     *
     * @param f             The computational action to perform.
     * @param precondition  A precondition of the action that can be used for
     * performing a sanity check on its input. Every value of type
     * <tt>T</tt> must satisfy this. The default value is
     * <tt>ADAPRO::Library::TOP</tt>, a predicate that is true for all values.
     * @param postcondition A postcondition of the action that can be used for
     * performing a sanity check on its output. Every value of type
     * <tt>U</tt> must satisfy this. The default value is
     * <tt>ADAPRO::Library::TOP</tt>, a predicate that is true for all values.
     * @return              The lifted return value.
     * @see ADAPRO::Library::TOP
     * @see ADAPRO::Library::Either::lift
     */
    inline Either then
    (
            const ADAPRO::Data::action_t<T, U>& f
#ifdef DEBUG
            , const ADAPRO::Data::predicate_t<T>& precondition = TOP<T>
            , const ADAPRO::Data::predicate_t<U>& postcondition = TOP<U>
#endif
    )
    noexcept
    {
        return is_right
                ? lift(f, right
#ifdef DEBUG
                        , precondition, postcondition
#endif
                )
                : make_left(left);
    }
};
}
}

#endif /* ADAPRO_EITHER_HPP */


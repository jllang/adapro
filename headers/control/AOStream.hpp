/*
 * File:   AOStream.hpp
 * Author: John Lång
 *
 * Created on 5 October 2016, 10:50
 */
#ifndef ADAPRO_AOSTREAM_HPP
#define ADAPRO_AOSTREAM_HPP

#include <string>
#include <mutex>
#include <queue>
#include "Thread.hpp"
#include "../data/State.hpp"

namespace ADAPRO
{
namespace Control
{
#ifndef VERIFY
    class Logger;
#endif

    /**
     * <p>AOStream stands for Asynchronous Output Stream. This abstract
     * templated class is the base class for the asynchronous output stream
     * classes of ADAPRO. AOStream follows the RAII (Resource Acquisition Is
     * Initialization) principle, which means that it automatically performs the
     * necessary preparations in its constructor, and necessary cleanups in its
     * destructor. Every public method of AOStream has a <tt>noexcept</tt>
     * guarantee, but the behaviour concerning exceptions is otherwise
     * undefined.</p>
     * <p>Asynchronousness of the stream put operation is achieved by a globally
     * locked internal queue. (AOStream could be optimized to use a ringbuffer
     * with individually locked entries, even though this costs more memory.)
     * </p>
     * <p>The implementations of this abstract class template must implement
     * <tt>prepare</tt>, <tt>execute</tt>, and <tt>stop</tt>.
     * </p>
     * <p>
     * <tt>prepare</tt> should contain all the necessary preparatory actions not
     * carried out in the constructors. When the control returns from this
     * procedure, the AOStream must be ready to perform the &lt;&lt; operation.
     * </p>
     * <p>
     * <tt>execute</tt> should contain take <em>one</em> (or more) <tt>T</tt>
     * from the protected queue and write it to the output source. It must also
     * use <tt>std::unique_lock</tt> for mutual exclusion on the queue using
     * <tt>aostream_mutex</tt> in order to prevent data race.
     * </p>
     * <p>
     * <tt>finish</tt> should perform the necessary operations for closing the
     * stream successfully. When control returns from this procedure, the
     * AOStream should be ready to be deleted. This procedure also needs to make
     * sure that the remaining contents of the queue are written into the output
     * source.
     * </p>
     * <p>
     * Also, the inheriting class has to call <tt>Thread::start()</tt> and
     * <tt>Thread::wait_for_state(ADAPRO::Data::State::RUNNING)</tt> in order to
     * ensure the correct functionality of the AOStream. The purpose of this
     * arrangement is to hide the Thread functionality from the public API and
     * make AOStream work in an easy, and RAII compliant manner. Also, the
     * implementing class has to call <tt>close</tt> in its destructor to ensure
     * that the AOStream gets closed and the Thread stopped properly.
     * </p>
     * @see ADAPRO::Control::Thread
     */
    template<typename T>
    class AOStream : protected Thread
    {

    // TODO: Try changing the queue to a ring buffer with individually locked
    // elements.

    protected:
        // TODO: The queue and mutex could be made private with protected inline
        // accessors.

        /**
         * The internal mutex of the AOStream.
         */
        std::mutex aostream_mutex;

        /**
         * The T queue of AOStream. The inheriting class should take one
         * <tt>T</tt> object of this queue and write it to the output source in
         * the virtual member procedure <tt>execute</tt>. This queue will be
         * written in the put to (&lt;&lt;) operator using an
         * <tt>std::unique_lock</tt> on <tt>aostream_mutex</tt>. <em>The
         * inheriting class must also use locking to prevent data race.</em>
         *
         * @see AOFStream::execute
         */
        std::queue<T> queue;

        /**
         * The deleted default cosntructor for AOStream.
         */
        AOStream() = delete;

        /**
         * Constructs an AOStream with the given name.
         *
         * @param logger            The Logger instance of an ADAPRO Session.
         * @param name              Name for the Thread object.
         * @param preferred_core    Preferred CPU core number. A negative value
         * indicates no preference. For more information, see
         * <tt>ADAPRO::Control::Thread</tt>.
         * @see ADAPRO::Control::Thread
         */
        AOStream
        (
#ifndef VERIFY
                Logger& logger,
#endif
                std::string&& name,
                const int preferred_core = -1
        )
        noexcept:
                Thread
                (
#ifndef VERIFY
                        logger,
#endif
                        std::move(name),
                        [](const ADAPRO::Data::State s){},
                        preferred_core
                ),
                aostream_mutex(),
                queue()
        {}
    public:

        /**
         * Writes the object to the stream asynchronously. This means that the
         * control might return before the actual output operation is finished.
         *
         * @param aostream  The stream used for writing.
         * @param object    The object to be written.
         * @return          A reference to the stream object.
         */
        inline void operator <<(const T& object) noexcept
        {
            // This operator appends object to the queue, that will be read one
            // <tt>T</tt> at a time in <tt>execute</tt>.
            std::unique_lock<std::mutex> lock(this->aostream_mutex);
            this->queue.push(object);
            lock.unlock();
        }

        /**
         * Closes the AOStream. When control returns from this procedure, every
         * T put to this stream should be written to the output source.
         */
        inline void close() noexcept
        {
            join();
        }

        virtual ~AOStream() noexcept {}

    };
}
}

#endif /* ADAPRO_AOSTREAM_HPP */


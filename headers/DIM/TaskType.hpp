/*
 * File:   TaskType.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 May 2017, 9:55
 */

#ifndef ADAPRO_DIM_TASK_TYPE_HPP
#define ADAPRO_DIM_TASK_TYPE_HPP

#include <cstdint>
#include <string>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"
#include "../data/State.hpp"

namespace ADAPRO
{
namespace DIM
{
    /**
     * TaskType is an enumeration used by <tt>ADAPRO::DIM::Controller</tt> for
     * identifying the appropriate <tt>ADAPRO::DIM::Wrapper</tt> API call when
     * performing a Task. The purpose of ADAPRO DIM task mechanism is to
     * centralize and serialize access to DIM C API, because it's not
     * thread-safe.
     *
     * @see ADAPRO::DIM::Controller
     */
    enum TaskType
    {
        /**
         * TaskType <tt>PUBLISH_SERVICE</tt> corresponds with an invocation of
         * <tt>ADAPRO::DIM::Wrapper::server_add_service</tt>.
         */
        PUBLISH_SERVICE,

        /**
         * TaskType <tt>PUBLISH_COMMAND</tt> corresponds with
         * <tt>ADAPRO::DIM::Wrapper::server_add_command</tt>.
         */
        PUBLISH_COMMAND,

        /**
         * TaskType <tt>UPDATE_SERVICE</tt> corresponds with
         * <tt>ADAPRO::DIM::Wrapper::server_update_service</tt>.
         */
        UPDATE_SERVICE,

        /**
         * TaskType <tt>SUBSCRIBE_TO_SERVICE</tt> corresponds with
         * <tt>ADAPRO::DIM::Wrapper::client_subscribe</tt> or
         * <tt>ADAPRO::DIM::Wrapper::client_subscribe_stamped</tt> depending on
         * the argument of the task given to Controller.
         */
        SUBSCRIBE_TO_SERVICE,

        /**
         * TaskType <tt>CALL_COMMAND</tt> corresponds with
         * <tt>ADAPRO::DIM::Wrapper::client_call_command</tt>.
         */
        CALL_COMMAND,

        /**
         * TaskType <tt>OVERWRITE_TIMESTAMP</tt> corresponds with
         * <tt>ADAPRO::DIM::Wrapper::client_write_timestamp</tt>.
         */
        OVERWRITE_TIMESTAMP
    };
}

namespace Library
{
    using namespace ADAPRO::DIM;

    /**
     * A straightforward conversion from <tt>ADAPRO::DIM::TaskType</tt> to
     * <tt>std::string</tt>. Each TaskType is mapped to its unqualified name in
     * upper case.
     *
     * @param state A TaskType.
     * @return      The corresponding string.
     * @throws std::domain_error If applied to an invalid value.
     */
    template<> inline std::string show(const TaskType task_type)
    {
        switch(task_type)
        {
            case PUBLISH_SERVICE:
                return "PUBLISH_SERVICE";
            case PUBLISH_COMMAND:
                return "PUBLISH_COMMAND";
            case UPDATE_SERVICE:
                return "UPDATE_SERVICE";
            case SUBSCRIBE_TO_SERVICE:
                return "SUBSCRIBE_TO_SERVICE";
            case CALL_COMMAND:
                return "CALL_COMMAND";
            case OVERWRITE_TIMESTAMP:
                return "OVERWRITE_TIMESTAMP";
            default:
                throw std::domain_error("Invalid TaskType.");
        }
    }

    /**
     * <p>Returns a bit mask of <tt>ADAPRO::Data::State</tt> values representing
     * the set of allowed states for <tt>ADAPRO::DIM::Controller</tt> to perform
     * a task of the given type. </p>
     * <p>For example, because Controller starts the DIM server during its own
     * startup sequence in state <tt>STARTING</tt>, all the services must be
     * added before this moment, namely when Controller is <tt>READY</tt>.</p>
     *
     * @param task_type A TaskType whose mask state mask is needed.
     * @return          The state mask of the given TaskType.
     * @see ADAPRO::Data::State
     * @see ADAPRO::DIM::Controller
     * @throws std::domain_error If applied to an invalid value.
     */
    inline uint8_t state_mask(const TaskType task_type)
    {
        switch(task_type)
        {
            case PUBLISH_SERVICE:
            case PUBLISH_COMMAND:
                return ADAPRO::Data::State::READY;
            case UPDATE_SERVICE:
            case OVERWRITE_TIMESTAMP:
                return ADAPRO::Data::State::RUNNING |
                        ADAPRO::Data::State::PAUSED;
            case SUBSCRIBE_TO_SERVICE:
            case CALL_COMMAND:
                return ADAPRO::Data::State::READY |
                        ADAPRO::Data::State::RUNNING |
                        ADAPRO::Data::State::PAUSED;
            default:
                throw std::domain_error("Invalid TaskType.");
        }
    }
}
}

#endif /* ADAPRO_DIM_TASK_TYPE_HPP */
/*
 * File:   Typedefs.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 May 2017, 11:07
 */

#ifndef ADAPRO_DIM_TYPEDEFS_HPP
#define ADAPRO_DIM_TYPEDEFS_HPP

#include <cstdint>
#include <string>
#include <functional>
#include <utility>
#include "TaskType.hpp"
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
/**
 * This namespace contains the DIM-related functionality of ADAPRO.
 */
namespace DIM
{
    struct TaskArgument;

    /**
     * <p>Type of callbacks used with some DIM facilities. The parameters
     * applied to this callback by DIM are:</p>
     * <ol>
     *     <li>
     *         A pointer to an integer value for identifying the DIM service
     *         associated with the callback. <em>This parameter is not the DIM
     *         service ID</em>. The value of this argument is set when
     *         registering the callback.
     *     </li>
     *     <li>
     *         A pointer to a binary data segment.
     *     </li>
     *     <li>
     *         A pointer to an <tt>int</tt> value that contains the size of the
     *         binary buffer, given as thesecond argument, in bytes.
     *     </li>
     * </ol>
     * <p>For more information, see the documentation of the procedure that uses
     * a dim command callback.</p>
     *
     * @see ADAPRO::Control::DIMWrapper::server_add_command
     * @see ADAPRO::Control::DIMWrapper::client_subscribe
     */
    using command_callback_t = void(*)(void*, void*, int*);

    /**
     * <p>Type of callback used for DIM error handlers. The parameters applied
     * to this callback by DIM are:</p>
     * <ol>
     *     <li>
     *         Severity of the error. In <tt>dim_common.h</tt> the severities
     *         are enumerated as follows:
     *         <tt>typedef enum { DIM_INFO, DIM_WARNING, DIM_ERROR, DIM_FATAL }DIM_SEVERITIES;</tt>
     *     </li>
     *     <li>
     *         An error code.
     *     </li>
     *     <li>
     *         The error message as a null-terminated char array.
     *     </li>
     * </ol>
     * <p>For more information, see the documentation of the procedure that uses
     * a dim error callback.</p>
     *
     * @see ADAPRO::Control::DIMWrapper::server_add_error_handler
     * @see ADAPRO::Control::DIMWrapper::client_add_error_handler
     */
    using error_callback_t = void(*)(int, int, char*);

    /**
     * Used for adding new DIM services. The purpose and arguments of this
     * callback is unclear.
     */
    using server_callback_t = void(*)(void*, void**, int*, int*);

    /**
     * <p>Type of a DIM command factory, i.e. a function taking a const
     * reference to a <tt>config_t</tt> object (that is
     * <tt>std::map&lt;std::string, std::string&gt;</tt>) and returning a tuple
     * containing information needed for publishing a DIM command service. The
     * expected return value is documented in
     * <tt>ADAPRO::Data::Context::dim_callbacks</tt>.</p>
     *
     * @see ADAPRO::Data::Context::dim_callbacks
     */
    using command_factory_t = std::function<
            std::tuple<const std::string, const std::string, command_callback_t>
            (const ADAPRO::Data::config_t&)>;

    /**
     * The type used for identifying DIM services.
     */
    using service_id_t = unsigned;

    /**
     * The type of task arguments for ADAPRO DIM Controller, i.e. a shared
     * pointer to a <tt>ADAPRO::DIM::ControllerParameter</tt>.
     */
    using task_argument_t = std::shared_ptr<TaskArgument>;

    /**
     * The type of tasks of ADAPRO DIM Controller, a pair containing an
     * enumerated task type and its corresponding argument.
     */
    using task_t = std::pair<TaskType, task_argument_t>;
}
}

#endif /* ADAPRO_DIM_TYPEDEFS_HPP */
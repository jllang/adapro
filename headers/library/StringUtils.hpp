/*
 * File:   StringUtils.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 14 July 2015, 10:37
 *
 * This library contains miscellaneous functions used in this program for
 * handling (C-style) strings.
 */

#ifndef ADAPRO_STRING_UTILS_HPP
#define	ADAPRO_STRING_UTILS_HPP

#include <string>
#include <vector>
#include <list>

namespace ADAPRO
{
#ifndef VERIFY
namespace Control
{
    class Logger;
}
#endif
namespace Library
{
    /**
     * Returns an uniformly distributed string of exactly the given length. The
     * alphabet of the string is ASCII characters between <tt>32</tt>
     * (<tt>' '</tt>) and <tt>126</tt> (<tt>'~'</tt>), inclusive.
     *
     * @param length    Length for the random string.
     * @return          The random string.
     * @deprecated      As of ADAPRO 5.0.0, this procedure has become
     * deprecated.
     */
    [[deprecated("This feature will be removed from ADAPRO in the future.")]]
    std::string random_string(const size_t length) noexcept;

    /**
     * Returns a random string of exactly the given length. The alphabet for the
     * string contains upper case letters, digits and the symbols <tt>'_'</tt>,
     * <tt>'/'</tt>, and <tt>'?'</tt>. Their distribution is geometric and tries
     * to roughly approximate the symbol frequencies in ALICE DCS DIM service
     * aliases.
     *
     * @param length    Length for the random string.
     * @return          The random string.
     * @deprecated      As of ADAPRO 5.0.0, this procedure has become
     * deprecated.
     */
    [[deprecated("This feature will be removed from ADAPRO in the future.")]]
    std::string random_string2(const size_t length) noexcept;

    /**
     * Calculates a hash code for the given string. Up to 52 first characters of
     * the string contribute to the hash code. The hash code is case
     * insensitive, so for example <tt>"abc"</tt> and <tt>"ABC"</tt> will have
     * colliding hash codes.
     *
     * @param input A string.
     * @return      An unsigned integer.
     * @deprecated  As of ADAPRO 5.0.0, this procedure has become deprecated.
     */
    [[deprecated("This feature will be removed from ADAPRO in the future.")]]
    uint64_t hash_code(const std::string& input) noexcept;

    /**
     * Copies a C-style array of command-line arguments to a single
     * space-separated standard string. The string shall be columnated to the
     * given offset of characters from the left, such that a line break
     * (<tt>'\n'</tt>) and an offset of spaces are inserted after parameters the
     * sum of the length of a parameter and offset is greater than or equal to
     * 80.
     *
     * @param argc      Number of strings to copy.
     * @param argv      The source of the strings.
     * @param offset    The offset for columnated output. Assumed to be well
     * less than 80.
    */
    std::string char_ptr_ptr_to_string
    (
            const int argc,
            char** argv,
            const size_t offset
    )
    noexcept;

    /**
     * Converts the given string into upper case. <em>This function will change
     * the state of the given string</em>, instead of returning a copy.
     *
     * @param str The string to be converted into upper case.
     */
    inline void to_upper_case(std::string& str) noexcept
    {
        for (char& c : str) {c = toupper(c);}
    }

    /**
     * Converts the given string into lower case. <em>This function will change
     * the state of the given string</em>, instead of returning a copy.
     *
     * @param str The string to be converted into lower case.
     */
    inline void to_lower_case(std::string& str) noexcept
    {
        for (char& c : str) {c = tolower(c);}
    }

    /**
     * Splits a string using the given separator and copies the resulting
     * substrings into a vector.
     *
     * @param source    The string to be splitted.
     * @param separator The delimiting character.
     * @return          A vector containing the substrings.
     */
    std::vector<std::string> split(const std::string& source,
            const char separator) noexcept;

    /**
     * Splits a string using whitespace as separator and copies the resulting
     * substrings to a vector. Only the non-whitespace substrings of the
     * argument will be returned.
     *
     * @param source    The string to be splitted.
     * @return          A vector containing the substrings.
     */
    std::vector<std::string> split_by_whitespace(
            const std::string& source) noexcept;

    /**
     * Returns a simple big endian hexadecimal presentation of the given
     * segment of memory.
     *
     * @param start     Address of a memory segment.
     * @param length    Length of the memory segment.
     * @return          A hexadecimal string representation of the segment in
     * bytes.
     */
    std::string to_hex_big_endian(const char* const start, size_t length)
    noexcept;

    /**
     * Returns a simple little endian hexadecimal presentation of the given
     * segment of memory.
     *
     * @param start     Address of a memory segment.
     * @param length    Length of the memory segment (in chars).
     * @return          A hexadecimal string representation of the segment in
     * bytes.
     */
    std::string to_hex_little_endian(const char* const start, size_t length)
    noexcept;

    /**
     * Prints the given list of key-value pairs.
     *
     * @param logger        The Logger instance used for output.
     * @param list_name     Name of the list to be printed as a heading.
     * @param parameters    The list to be printed
     * @param offset        A number of characters from the left to append as an
     * offset before values. Used for columnating output.
     */
    void print_k_v_list
    (
#ifndef VERIFY
            ADAPRO::Control::Logger& logger,
#endif
            const std::string& list_name,
            const std::list<std::pair<std::string, std::string>>& parameters,
            const size_t offset
    )
    noexcept;
}
}

#endif	/* ADAPRO_STRING_UTILS_HPP */

